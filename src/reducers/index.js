import LanguageReducer from './LanguageReducer';
import AuthReducer from './AuthReducer';
import { combineReducers } from "redux";


export default combineReducers({
    lang: LanguageReducer,
    auth: AuthReducer
});