import firebase from 'firebase';

export const initializeFirebase = () => {
    var config = {
      apiKey: "AIzaSyDagNQpmQMLI58Eh4veHT5Sn1wRqJDirJE",
      authDomain: "qsatha-9c7f1.firebaseapp.com",
      databaseURL: "https://qsatha-9c7f1.firebaseio.com",
      projectId: "qsatha-9c7f1",
      storageBucket: "qsatha-9c7f1.appspot.com",
      messagingSenderId: "235125235878",
      appId: "1:235125235878:web:659da3167a299e93"
      };
    firebase.initializeApp(config);
  }

  export const askForPermissioToReceiveNotifications = async () => {
    try {
  
      const messaging = firebase.messaging();
  
      await messaging.requestPermission();
      const token = await messaging.getToken();
      console.log('user token: ', token);
  
      return token;
    } catch (error) {
      console.error(error);
    }
  }

  



  
