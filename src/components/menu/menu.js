import React, { Component } from 'react';
import './menu.css';
import img from './image.png';
import {NavLink} from 'react-router-dom';
import { connect } from 'react-redux'
import  {allStrings} from '../../assets/strings'
import {Button, SideNav ,SideNavItem,Icon,Collapsible,CollapsibleItem} from 'react-materialize'

class Menu extends Component {
  constructor(props){
    super(props)
    if(this.props.isRTL){
      allStrings.setLanguage('ar')
    }else{
      allStrings.setLanguage('en')
    }
  }
  render() {
    return (
      <SideNav 
      trigger={<Button className='menu-btn'><Icon>menu</Icon></Button>}
      options={{ closeOnClick: true }}
      >
        <SideNavItem  userView
          user={{
            //background: 'img',
            
            image: require('../../assets/images/qstLogo2.png'),
            name: 'Qasat Store',
          }}
        />
        <br></br>
        <SideNavItem divider style={{background:"#333"}} />
        <br></br>

        <NavLink to="/Dashboard">
            <SideNavItem waves href='#' icon='dashboard'>
                {allStrings.dashboard}
            </SideNavItem>
        </NavLink>

        <Collapsible>
          <CollapsibleItem header={allStrings.user} icon='supervised_user_circle'>
          <NavLink to='/users'>
            <SideNavItem waves icon='group'>
            {allStrings.user}
            </SideNavItem>
        </NavLink>

        <NavLink to='/sales-man'>
            <SideNavItem waves icon='group'>
            {allStrings.salesman}
            </SideNavItem>
        </NavLink>

          </CollapsibleItem>
        </Collapsible>

        <NavLink to='/premiums'>
            <SideNavItem waves icon='add_to_photos'>
            {allStrings.permiums}
            </SideNavItem>
        </NavLink>

        <Collapsible>
          <CollapsibleItem header={allStrings.order} icon='add_shopping_cart'>
          
            <NavLink to='/acceptOrder'>
                <SideNavItem waves icon='local_mall'>
                    {allStrings.acceptOrders}
                </SideNavItem>
            </NavLink>
            <NavLink to='/pendingOrder'>
              <SideNavItem waves icon='local_mall'>
              {allStrings.pendingOrder}
              </SideNavItem>
            </NavLink>
            <NavLink to='/onTheWayOrder'>
              <SideNavItem waves icon='local_mall'>
              {allStrings.onTheWayORder}
              </SideNavItem>
            </NavLink>
            <NavLink to='/deliverdOrder'>
              <SideNavItem waves icon='local_mall'>
              {allStrings.deliverdOrder}
              </SideNavItem>
            </NavLink>
          </CollapsibleItem>
        </Collapsible>
        <NavLink to='/products'>
          <SideNavItem waves icon='shopping_basket'>
          {allStrings.product}
          </SideNavItem>
        </NavLink>
        <NavLink to='/category'>
            <SideNavItem waves icon='turned_in_not'>
            {allStrings.category}
            </SideNavItem>
        </NavLink>
        <NavLink to='/Ads'>
            <SideNavItem waves icon='add_to_queue'>
            {allStrings.ads}
            </SideNavItem>
        </NavLink>
        <NavLink to='/partners'>
            <SideNavItem waves icon='group'>
            {allStrings.partners}
            </SideNavItem>
        </NavLink>
        <NavLink to='/branch'>
            <SideNavItem waves icon='widgets'>
            {allStrings.branches}
            </SideNavItem>
        </NavLink>
       
        <NavLink to='/reports'>
              <SideNavItem waves icon='account_circle'>
              {allStrings.reports} 
              </SideNavItem>
        </NavLink>
<br></br>
<br></br>
        
    </SideNav>
    );
  }
}
const mapToStateProps = state => ({
  isRTL: state.lang.isRTL,
  currentUser: state.auth.currentUser,

})

const mapDispatchToProps = {
}

export default connect(mapToStateProps,mapDispatchToProps) (Menu);
