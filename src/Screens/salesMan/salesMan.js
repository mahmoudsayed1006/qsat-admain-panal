import React, { Component } from 'react';
import Menu from '../../components/menu/menu';
import Nav from '../../components/navbar/navbar';
import Tables from '../../components/table/table';
import Footer from '../../components/footer/footer';

import './salesMan.css';
//import {Icon,Button,Modal,Input} from 'react-materialize';
import {Skeleton, Modal, Form, Icon, Input, Button,Select,Popconfirm, message} from 'antd';
import "antd/dist/antd.css";
import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import {withRouter} from 'react-router-dom'
import { connect } from 'react-redux'
import  {allStrings} from '../../assets/strings'


class SalesMan extends React.Component {
   

    constructor(props){
        super(props)
        this.getSalesMen(1)
        if(this.props.isRTL){
          allStrings.setLanguage('ar')
        }else{
          allStrings.setLanguage('en')
        }
      }
      pagentationPage=0;
      counter=0;
    state = {
        visible: false,
        confirmDelete: false,
        selectedUser:null,
        salesmen:[],
        loading:true
        }
       //submit form
       flag = -1;
       getSalesMen = (page,deleteRow) => {
         axios.get(`${BASE_END_POINT}find?type=SALES-MAN&page=${page}&limit={20}`)
         .then(response=>{
           //console.log("ALL salesmen")
           //console.log(response.data)
           this.setState({salesmen:deleteRow?response.data.data:[...this.state.salesmen,...response.data.data],loading:false})
         })
         .catch(error=>{
          // console.log("ALL salesmen ERROR")
          // console.log(error.response)
         })
       }

       OKBUTTON = (e) => {
        this.deleteUser()
       }
 
       deleteUser = () => {
         let l = message.loading('Wait..', 2.5)
         axios.delete(`${BASE_END_POINT}${this.state.selectedUser}/delete`,{
           headers: {
             'Content-Type': 'application/x-www-form-urlencoded',
             'Authorization': `Bearer ${this.props.currentUser.token}`
           },
         })
         .then(response=>{
             l.then(() => message.success('User Deleted', 2.5))
             this.getSalesMen(1,true)
             this.flag = -1
             
         })
         .catch(error=>{
            // console.log(error.response)
             l.then(() => message.error('Error', 2.5))
         })
      }
    //submit form
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
          if (!err) {
            //console.log('Received values of form: ', values);
            //form.append('jobLocation', values.jobLocation);
            const data = {
                firstname: values.firstname,
                lastname: values.lastname,
                cardNum: values.cardNum,
                address: values.address,
                area: values.area,
                email: values.email,
                phone: values.phone,
                password: values.password,
                type: values.type.key,
                jobLocation:values.jobLocation,
                job:values.job
                
            }
            let l = message.loading('Wait..', 2.5)
            axios.post(`${BASE_END_POINT}signup`,JSON.stringify(data),{
              headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.props.currentUser.token}`
              },
            })
            .then(response=>{
                l.then(() => message.success('Add Sales-man', 2.5));
                this.setState({ modal1Visible:false });
                this.getSalesMen(1,true)
                this.flag = -1
            })
            .catch(error=>{
              //  console.log(error.response)
                l.then(() => message.error('Error', 2.5))
            })
          }
        });
        
      }
     
      

    showModal = () => {
      this.setState({
        visible: true,
      });
    }
  
      setModal1Visible(modal1Visible) {
        this.setState({ modal1Visible });
      }
  //end modal
    render() {
        //form
         const { getFieldDecorator } = this.props.form;
         //select
         const Option = Select.Option;

         function handleChange(value) {
            //console.log(value); 
         }
         //end select
         let controls = (
            <Popconfirm
            title={allStrings.areusure}
            onConfirm={this.OKBUTTON}
            onCancel={this.fCANCELBUTTON}
            okText={allStrings.ok}
            cancelText={allStrings.cancel}
          >
             <Icon className='controller-icon' type="delete" />
          </Popconfirm>
         )
         let list = this.state.salesmen.map(val=>[val.id,val.firstname,val.lastname,val.phone[0],val.email,val.cardNum,""+val.address,""+val.area,""+val.job,""+val.jobLocation,val.active?'نشط':'غير نشط'])
         
          list.forEach(function(row) {
            row.push(controls)
           });
           const loadingView = [
            [<Skeleton  active/> ],
            [<Skeleton active/> ],
            [<Skeleton  active/> ],
            [<Skeleton active/> ],
            
           ]
      return (
          <div>
              <Menu></Menu>
              <Nav></Nav>
              <Tables columns={this.state.loading?['Loading...']:[allStrings.id,allStrings.firstname,allStrings.lastname, allStrings.phone,allStrings.email,allStrings.cardnumber,allStrings.address,allStrings.area,allStrings.job,allStrings.jobLocation,allStrings.active,allStrings.remove]} title={allStrings.salesmantable}
              arr={this.state.loading?loadingView:list}
              onCellClick={(colData,cellMeta,)=>{
                //console.log('col index  '+cellMeta.colIndex)
               // console.log('row index   '+colData)
                if(cellMeta.colIndex==0){
                  
                  //console.log(this.state.salesmen[cellMeta.rowIndex])
                  this.props.history.push('/SalesManInfo',{data:this.state.salesmen[this.pagentationPage+cellMeta.rowIndex]})
                }else if(cellMeta.colIndex==11){
                    const id = list[this.pagentationPage + cellMeta.rowIndex][0];
                    this.setState({selectedUser:id})
                    //console.log(id)
                  }
              }}

               onChangePage={(currentPage)=>{
                if(currentPage>this.counter){
                  this.counter=currentPage;
                  this.pagentationPage=this.pagentationPage+10
                }else{
                 this.counter=currentPage;
                 this.pagentationPage=this.pagentationPage-10
                }
                //console.log(currentPage)
                if(currentPage%2!=0  && currentPage > this.flag){
                  this.getSalesMen(currentPage+1)
                  this.flag  = currentPage;
                 
                }
                  
              }}
              >
              </Tables>
              <div>
              <Button style={{color: 'white', backgroundColor:'#25272e', marginLeft:60}} onClick={() => this.setModal1Visible(true)}>{allStrings.addsalesman}</Button>
              <Modal
                    title="Add"
                    visible={this.state.modal1Visible}
                    onOk={this.handleSubmit}
                    cancelText={allStrings.cancel}
                    okText={allStrings.ok}
                    onCancel={() => this.setModal1Visible(false)}
                  >
                    <Form onSubmit={this.handleSubmit} className="login-form">
                    <Form.Item>
                    {getFieldDecorator('firstname', {
                        rules: [{ required: true, message: 'Please enter First Name' }],
                    })(
                        <Input placeholder={allStrings.firstname} />
                    )}
                    </Form.Item>
                    <Form.Item>
                    {getFieldDecorator('lastname', {
                        rules: [{ required: true, message: 'Please enter last name' }],
                    })(
                        <Input placeholder={allStrings.lastname} />
                    )}
                    </Form.Item>
                   
                    <Form.Item>
                    {getFieldDecorator('email', {
                        rules: [{ required: true, message: 'Please enter email' }],
                    })(
                        <Input placeholder={allStrings.email} />
                    )}
                    </Form.Item>
                    <Form.Item>
                    {getFieldDecorator('phone', {
                        rules: [{ required: true, message: 'Please enter phone' }],
                    })(
                        <Input placeholder={allStrings.phone} />
                    )}
                    </Form.Item>
                    <Form.Item>
                    {getFieldDecorator('password', {
                        rules: [{ required: true, message: 'Please enter password' }],
                    })(
                        <Input placeholder={allStrings.password} type="Password" />
                    )}
                    </Form.Item>
                    <Form.Item>
                    {getFieldDecorator('type', {
                        rules: [{ required: true, message: 'Please enter type' }],
                    })(
                        <Select labelInValue  
                        placeholder={allStrings.type}
                        style={{ width: '100%'}} onChange={handleChange}>
                            <Option value="SALES-MAN">{allStrings.salesman}</Option>
                        </Select>
                    )}
                    </Form.Item>
                    <Form.Item>
                    {getFieldDecorator('cardNum', {
                        rules: [{ required: true, message: 'Please enter card Number' }],
                    })(
                        <Input placeholder={allStrings.cardnumber} />
                    )}
                    </Form.Item>
                    <Form.Item>
                    {getFieldDecorator('address', {
                        rules: [{ required: false, message: 'Please enter address' }],
                    })(
                        <Input placeholder={allStrings.address} />
                    )}
                    </Form.Item>
                    <Form.Item>
                    {getFieldDecorator('area', {
                        rules: [{ required: false, message: 'Please enter area' }],
                    })(
                        <Input placeholder={allStrings.area} />
                    )}
                    </Form.Item>
                    <Form.Item>
                    {getFieldDecorator('job', {
                        rules: [{ required: false, message: 'Please enter job' }],
                    })(
                        <Input placeholder={allStrings.job} />
                    )}
                    </Form.Item>
                    <Form.Item>
                    {getFieldDecorator('jobLocation', {
                        rules: [{ required: false, message: 'Please enter jobLocation' }],
                    })(
                        <Input placeholder={allStrings.jobLocation} />
                    )}
                    </Form.Item>
                    </Form>
                </Modal>
            {/*}   <Modal
                  title="Edit"
                  visible={this.state.modal2Visible}
                  onOk={this.handleSubmit2}
                  onCancel={() => this.setModal2Visible(false)}
                >
                  <Form onSubmit={this.handleSubmit2} className="login-form">
                  <Form.Item>
                    {getFieldDecorator('firstname', {
                        rules: [{ required: true, message: 'Please enter First Name' }],
                    })(
                        <Input placeholder="First Name" />
                    )}
                    </Form.Item>
                    <Form.Item>
                    {getFieldDecorator('lastname', {
                        rules: [{ required: true, message: 'Please enter last name' }],
                    })(
                        <Input placeholder="Last Name" />
                    )}
                    </Form.Item>
                   
                    <Form.Item>
                    {getFieldDecorator('email', {
                        rules: [{ required: true, message: 'Please enter email' }],
                    })(
                        <Input placeholder="email" />
                    )}
                    </Form.Item>
                    <Form.Item>
                    {getFieldDecorator('phone', {
                        rules: [{ required: true, message: 'Please enter phone' }],
                    })(
                        <Input placeholder="phone" />
                    )}
                    </Form.Item>
                    <Form.Item>
                    {getFieldDecorator('password', {
                        rules: [{ required: true, message: 'Please enter password' }],
                    })(
                        <Input placeholder="password" type="Password" />
                    )}
                    </Form.Item>
                    <Form.Item>
                    {getFieldDecorator('type', {
                        rules: [{ required: true, message: 'Please enter type' }],
                    })(
                        <Select labelInValue defaultValue={{ key: 'SALES-MAN' }} 
                        placeholder="Select a option"
                        style={{ width: '100%'}} onChange={handleChange}>
                            <Option value="SALES-MAN">Sales Man</Option>
                            <Option value="ADMIN">Admin</Option>
                        </Select>
                    )}
                    </Form.Item>
                    <Form.Item>
                    {getFieldDecorator('cardNum', {
                        rules: [{ required: true, message: 'Please enter card Number' }],
                    })(
                        <Input placeholder="Card Number" />
                    )}
                    </Form.Item>
                    <Form.Item>
                    {getFieldDecorator('job', {
                        rules: [{ required: true, message: 'Please enter job' }],
                    })(
                        <Input placeholder="job" />
                    )}
                    </Form.Item>
                    <Form.Item>
                    {getFieldDecorator('jobLocation', {
                        rules: [{ required: true, message: 'Please enter job location' }],
                    })(
                        <Input placeholder="job loaction" />
                    )}
                    </Form.Item>
                    <Form.Item>
                    {getFieldDecorator('address', {
                        rules: [{ required: false, message: 'Please enter address' }],
                    })(
                        <Input placeholder="address" />
                    )}
                    </Form.Item>
                    <Form.Item>
                    {getFieldDecorator('area', {
                        rules: [{ required: false, message: 'Please enter area' }],
                    })(
                        <Input placeholder="area" />
                    )}
                    </Form.Item>
                  </Form>
                </Modal>
                    {*/}
              </div>
             <Footer></Footer>
          </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,

  })
  
  const mapDispatchToProps = {
  }

  export default withRouter( connect(mapToStateProps,mapDispatchToProps) (SalesMan = Form.create({ name: 'normal_login' })(SalesMan)) );

