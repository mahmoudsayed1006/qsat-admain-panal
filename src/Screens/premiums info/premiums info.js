import React from 'react';
import Menu from '../../components/menu/menu';
import Nav from '../../components/navbar/navbar';
import Footer from '../../components/footer/footer';

import './premiums info.css';
import { message,Modal, Form,Select} from 'antd';

import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL';
import "antd/dist/antd.css";
import {Table} from 'react-materialize'
import {withRouter} from 'react-router-dom'
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'


class PremiumsInfo extends React.Component {
    state = {
        modal1Visible: false,
        premiums:this.props.location.state.data,
        salesman:[],

    }

    constructor(props){
        super(props)
        if(this.props.isRTL){
          allStrings.setLanguage('ar')
        }else{
          allStrings.setLanguage('en')
        }        
      }
      componentDidMount(){
         this.getSalesman()
      }
      getSalesman = () => {
        axios.get(`${BASE_END_POINT}find?type=SALES-MAN`)
        .then(response=>{
          //console.log("ALL SalesMAn")
          //console.log(response.data)
          this.setState({salesman:response.data.data})
        })
        .catch(error=>{
          //console.log("ALL SalesMAn ERROR")
          console.log(error.response)
        })
      }
    deletePremiums = () => {
        let l = message.loading('Wait..', 2.5)
        axios.delete(`${BASE_END_POINT}premiums/${this.state.premiums.id}`,{
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        })
        .then(response=>{
            l.then(() => message.success('premium Deleted', 2.5));
            this.props.history.goBack()
        })
        .catch(error=>{
            //console.log(error.response)
            l.then(() => message.error('Error', 2.5))
        })
     }
     finishPremium = () => {
      
        let l = message.loading('Wait..', 2.5)
         axios.put(`${BASE_END_POINT}premiums/${this.state.premiums.id}/finish`,{},{
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        })
         .then(response=>{
              //console.log('done')
              l.then(() => message.success('premium finish', 2.5))
              this.props.history.goBack()
         })
         .catch(error=>{
          //console.log('Error')
          //console.log(error.response)
          l.then(() => message.error('Error', 2.5))
         })
     }
     //submit form
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
          if (!err) {
            //console.log('Received values of form: ', values);
            const data = {
            }
            if(values.oldSalesMan){
                data.oldSalesMan = values.oldSalesMan.key
            }
            if(values.newSalesMan){
                data.newSalesMan = values.newSalesMan.key
            }
           
            //console.log(data)
            
            let l = message.loading('Wait..', 2.5)
          axios.put(`${BASE_END_POINT}premiums/${this.state.premiums.id}/updateSalesMan`,JSON.stringify(data),{
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${this.props.currentUser.token}`
            },
          })
          .then(response=>{
              l.then(() => message.success('salesMan Changed', 2.5))
              this.setState({ visible:false });
              this.props.history.goBack()
          })
          .catch(error=>{
              //console.log(error.response)
              l.then(() => message.error('Error', 2.5))
          })
          
          }
        });
        
      }
     setModal1Visible(modal1Visible) {
        this.setState({ modal1Visible });
      }
    render() {
        const {premiums} = this.state
        const { getFieldDecorator } = this.props.form;
        //select
        const Option = Select.Option;

       
        //end select
      return (
          
        <div>
         <Menu></Menu>
              <Nav></Nav>
        <div className='login'>
        <div class="row">
            <div class="col m2">
                <div class='title' style={{backgroundColor:'#25272e'}} >
                    <h2 class="center-align" style={{color:'#fff'}}>{allStrings.premiumsinfo}</h2>
                </div>
                <div class="row">
                    <form class="col s12">
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="totalMonth" type="text" class="validate" disabled value={premiums.totalMonth}>
                            </input>
                            <label for="totalMonth" class="active">{allStrings.totalMonth}</label>
                            </div>
                            <div class="input-field col s6">
                            <input id="monthRemaining" type="text" class="validate" disabled value={premiums.monthRemaining}></input>
                            <label for="monthRemaining" class="active">{allStrings.monthRemaining}</label>
                            </div>
                        </div>

                        <div className='dash-table'>
                            <h5>{allStrings.client}</h5>
                            <div className='row'>
                                <div className="col s6 m6 l6 dashboard-table">
                                    <Table>
                                        <thead>
                                            <tr>
                                            <th data-field="id">{allStrings.id}</th>
                                            <th data-field="name">{allStrings.name}</th>
                                            <th data-field="email">{allStrings.email}</th>
                                            <th data-field="mobile">{allStrings.phone}</th>
                                            <th data-field="address">{allStrings.address}</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <tr onClick={()=>{
                                                this.props.history.push('/UserInfo',{data:this.state.premiums.client})
                                                }}>
                                                <td>{premiums.client.id}</td>
                                                <td>{premiums.client.firstname +  premiums.client.lastname}</td>
                                                <td>{premiums.client.email}</td>
                                                <td>{premiums.client.phone[0]}</td>
                                                <td>{premiums.client.address}</td>
                                            </tr>
                                        </tbody>
                                    </Table>
                                </div>
                            </div>
                        </div>
                        <div className='dash-table'>
                            <h5>{allStrings.salesman}</h5>
                            <div className='row'>
                                <div className="col s6 m6 l6 dashboard-table">
                                    <Table>
                                        <thead>
                                            <tr>
                                            <th data-field="id">{allStrings.id}</th>
                                            <th data-field="name">{allStrings.name}</th>
                                            <th data-field="email">{allStrings.email}</th>
                                            <th data-field="mobile">{allStrings.phone}</th>
                                            <th data-field="address">{allStrings.address}</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <tr onClick={()=>{
                                                this.props.history.push('/SalesManInfo',{data:this.state.premiums.salesMan})
                                                }}>
                                                <td>{premiums.salesMan.id}</td>
                                                <td>{premiums.salesMan.firstname +  premiums.salesMan.lastname}</td>
                                                <td>{premiums.salesMan.email}</td>
                                                <td>{premiums.salesMan.phone[0]}</td>
                                                <td>{premiums.salesMan.address}</td>
                                            </tr>
                                        </tbody>
                                    </Table>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="costRemaining" type="text" class="validate" disabled disabled value={premiums.costRemaining}>
                            </input>
                            <label for="costRemaining" class="active">{allStrings.costRemaining}</label>
                            </div>
                            <div class="input-field col s6">
                            <input id="paidCost" type="text" class="validate" disabled disabled value={premiums.paidCost}></input>
                            <label for="paidCost" class="active">{allStrings.paidcost}</label>
                            </div>
                        </div>

                        <div className='dash-table'>
                            <h5>{allStrings.product}</h5>
                            <div className='row'>
                                <div className="col s6 m6 l6 dashboard-table">
                                    <Table>
                                        <thead>
                                            <tr>
                                            <th data-field="id">{allStrings.id}</th>
                                            <th data-field="name">{allStrings.name}</th>
                                            <th data-field="company">{allStrings.company}</th>
                                            <th data-field="category">{allStrings.category}</th>
                                            <th data-field="price">{allStrings.price}</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <tr onClick={()=>{
                                                this.props.history.push('/ProductInfo',{data:this.state.premiums.product})
                                                }}>
                                                <td>{premiums.product.id}</td>
                                                <td>{premiums.product.name}</td>                                 
                                                <td>{premiums.product.company}</td>
                                                <td>{premiums.product.category[0].categoryname}</td>
                                                <td>{premiums.product.installmentPrice}</td>
                                            </tr>
                                        </tbody>
                                    </Table>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                            <input id="paidDate" type="text" class="validate" disabled disabled value={premiums.paidDate.substring(0, 10)}></input>
                            <label for="paidDate" class="active">{allStrings.paidDate}</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                            <input id="costPerMonth" type="text" class="validate" disabled value={premiums.costPerMonth}></input>
                            <label for="costPerMonth" class="active">{allStrings.costPerMonth}</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                            <input id="notes" type="text" class="validate" disabled value={premiums.notes}></input>
                            <label for="notes" class="active">{allStrings.notes}</label>
                            </div>
                        </div>
                            <a class="waves-effect waves-light btn btn-large delete"  onClick={this.deletePremiums}><i class="spcial material-icons left delete">delete</i>{allStrings.remove}</a>
                            <a class="waves-effect waves-light btn btn-large"  onClick={this.finishPremium}><i class="spcial material-icons left">done</i>{allStrings.finish}</a>
                            <a class="waves-effect waves-light btn btn-large edit"  onClick={() => this.setModal1Visible(true)}><i class="spcial material-icons left">edit</i>{allStrings.changeSalesMan}</a>
                            <div>
                            <Modal
                                title={allStrings.changeSalesMan}
                                visible={this.state.modal1Visible}
                                onOk={this.handleSubmit}
                                onCancel={() => this.setModal1Visible(false)}
                                okText={allStrings.ok}
                                cancelText={allStrings.cancel}
                              >
                                <Form onSubmit={this.handleSubmit} className="login-form">
                                    <Form.Item>
                                        {getFieldDecorator('oldSalesMan', {
                                            rules: [{ required: true, message: 'Please enter old sales Man' }],
                                        })(
                                            <Select labelInValue  
                                            placeholder={allStrings.oldsalesman}
                                            style={{ width: '100%'}} >
                                                {this.state.salesman.map(item=>
                                                  <Option value={item.id}>{item.firstname +" "+ item.lastname}</Option>
                                                )}
                                           
                                            </Select>
                                        )}
                                    </Form.Item>
                                    <Form.Item>
                                        {getFieldDecorator('newSalesMan', {
                                            rules: [{ required: true, message: 'Please enter new sales Man' }],
                                        })(
                                            <Select labelInValue 
                                            placeholder={allStrings.newsalesman}
                                            style={{ width: '100%'}} >
                                                {this.state.salesman.map(item=>
                                                  <Option value={item.id}>{item.firstname +" "+ item.lastname}</Option>
                                                )}
                                           
                                            </Select>
                                        )}
                                    </Form.Item>
                                  
                                </Form>
                            </Modal>

                                
                        
                            </div>
                        </form>

                        
                    </div>
            </div>
        </div>
        </div>
        <Footer></Footer>
    </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,

  })
  
  const mapDispatchToProps = {
  }
  export default connect(mapToStateProps,mapDispatchToProps)(PremiumsInfo = Form.create({ name: 'normal_login', })(PremiumsInfo)) ;
