
import React from 'react';
import Menu from '../../components/menu/menu';
import Nav from '../../components/navbar/navbar';
import Tables from '../../components/table/table';
import Footer from '../../components/footer/footer';

import './product.css';
//import {Icon,Button,Modal,Input} from 'react-materialize';
import { Skeleton,Modal, Form, Icon, Input, Button, Popconfirm, message,Select} from 'antd';
import "antd/dist/antd.css";
import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import {withRouter} from 'react-router-dom'
import { connect } from 'react-redux'
import  {allStrings} from '../../assets/strings'

class Product extends React.Component {

    pagentationPage=0;
    counter=0;
    state = {
        modal1Visible: false,
        confirmDelete: false,
        selectedProduct:null,
        products:[],
        file: null,
        categories: [],
        loading:true,
        num_Inputs:0,
        }

        constructor(props){
          super(props)
          if(this.props.isRTL){
            allStrings.setLanguage('ar')
          }else{
            allStrings.setLanguage('en')
          }
        }

        onChange = (e) => {
            //console.log(Array.from(e.target.files))
            this.setState({file:e.target.files});
           // console.log(e.target.files[0])
        }
       //submit form
       flag = -1;
       getProducts = (page,deleteRow) => {
         axios.get(`${BASE_END_POINT}products?page=${page}&limit={20}`)
         .then(response=>{
           //console.log("ALL products")
          // console.log(response.data.data)
           this.setState({products:deleteRow?response.data.data:[...this.state.products,...response.data.data],loading:false})
         })
         .catch(error=>{
          // console.log("ALL products ERROR")
           console.log(error.response)
         })
       }

      flag = -1;
       getCategories = (page) => {
        axios.get(`${BASE_END_POINT}categories?page=${page}&limit={20}`)
        .then(response=>{
        //  console.log("ALL Categories")
        //  console.log(response.data)
          this.setState({categories:response.data.data})
        })
        .catch(error=>{
       //   console.log("ALL Categories ERROR")
          console.log(error.response)
        })
      }

       componentDidMount(){
        // console.log(this.props.currentUser)
         this.getProducts(1)
         this.getCategories()
       }

       OKBUTTON = (e) => {
       this.deleteProduct()
      }

      deleteProduct = () => {
        let l = message.loading('Wait..', 2.5)
        axios.delete(`${BASE_END_POINT}products/${this.state.selectedProduct}`,{
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        })
        .then(response=>{
            l.then(() => message.success('Product Deleted', 2.5));
            this.setState({ modal1Visible:false });
            this.getProducts(1,true)
            this.flag = -1
        })
        .catch(error=>{
           // console.log(error.response)
            l.then(() => message.error('Error', 2.5))
        })
     }
     
       
    //submit form
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
          if (!err) {
           // console.log('Received values of form: ', values);
          //  console.log(this.state.file);
            var form = new FormData();
           // form.append('img',this.state.file[0])
            
            for(var i=0 ; i<= this.state.file.length-1 ; i++)
            {
                form.append(`img`,this.state.file[i] )
                
             // console.log("ooo");
          }
          
            let arr = [];
            /*arr[0] = {attr:"property1",value:""}
            arr[1] = {attr:'property2',value:""}
            arr[2] = {attr:"property3",value:""}
            arr[3] = {attr:"property4",value:""}
            arr[4] = {attr:"property5",value:""}
            arr[5] = {attr:"property6",value:""}*/

            if(this.state.num_Inputs==1){
              arr[0] = {attr:values.key1,value:values.value1}
            }

            if(this.state.num_Inputs==2){
              arr[0] = {attr:values.key1,value:values.value1}
              arr[1] = {attr:values.key2,value:values.value2}
            }

            if(this.state.num_Inputs==3){
              arr[0] = {attr:values.key1,value:values.value1}
              arr[1] = {attr:values.key2,value:values.value2}
              arr[2] = {attr:values.key3,value:values.value3}
            
            }

            if(this.state.num_Inputs==4){
              arr[0] = {attr:values.key1,value:values.value1}
              arr[1] = {attr:values.key2,value:values.value2}
              arr[2] = {attr:values.key3,value:values.value3}     
              arr[3] = {attr:values.key4,value:values.value4}
            
            }

            if(this.state.num_Inputs==5){
              arr[0] = {attr:values.key1,value:values.value1}
              arr[1] = {attr:values.key2,value:values.value2}
              arr[2] = {attr:values.key3,value:values.value3}
              arr[3] = {attr:values.key4,value:values.value4}
              arr[4] = {attr:values.key5,value:values.value5}
            
            }

            if(this.state.num_Inputs==6){
              arr[0] = {attr:values.key1,value:values.value1}
              arr[1] = {attr:values.key2,value:values.value2}
              arr[2] = {attr:values.key3,value:values.value3}
              arr[3] = {attr:values.key4,value:values.value4}
              arr[4] = {attr:values.key5,value:values.value5}
              arr[5] = {attr:values.key6,value:values.value6}
            
            }

            if(this.state.num_Inputs==7){
              arr[0] = {attr:values.key1,value:values.value1}
              arr[1] = {attr:values.key2,value:values.value2}
              arr[2] = {attr:values.key3,value:values.value3}
              arr[3] = {attr:values.key4,value:values.value4}
              arr[4] = {attr:values.key5,value:values.value5}
              arr[5] = {attr:values.key6,value:values.value6}
              arr[6] = {attr:values.key7,value:values.value7}
            
            }

            if(this.state.num_Inputs==8){
              arr[0] = {attr:values.key1,value:values.value1}
              arr[1] = {attr:values.key2,value:values.value2}
              arr[2] = {attr:values.key3,value:values.value3}
              arr[3] = {attr:values.key4,value:values.value4}
              arr[4] = {attr:values.key5,value:values.value5}
              arr[5] = {attr:values.key6,value:values.value6}
              arr[6] = {attr:values.key7,value:values.value7}
              arr[7] = {attr:values.key8,value:values.value8}
            
            }

            if(this.state.num_Inputs==9){
              arr[0] = {attr:values.key1,value:values.value1}
              arr[1] = {attr:values.key2,value:values.value2}
              arr[2] = {attr:values.key3,value:values.value3}
              arr[3] = {attr:values.key4,value:values.value4}
              arr[4] = {attr:values.key5,value:values.value5}
              arr[5] = {attr:values.key6,value:values.value6}
              arr[6] = {attr:values.key7,value:values.value7}
              arr[7] = {attr:values.key8,value:values.value8}
              arr[8] = {attr:values.key9,value:values.value9}
            
            }

            if(this.state.num_Inputs==10){
              arr[0] = {attr:values.key1,value:values.value1}
              arr[1] = {attr:values.key2,value:values.value2}
              arr[2] = {attr:values.key3,value:values.value3}
              arr[3] = {attr:values.key4,value:values.value4}
              arr[4] = {attr:values.key5,value:values.value5}
              arr[5] = {attr:values.key6,value:values.value6}
              arr[6] = {attr:values.key7,value:values.value7}
              arr[7] = {attr:values.key8,value:values.value8}
              arr[8] = {attr:values.key9,value:values.value9}
              arr[9] = {attr:values.key10,value:values.value10}
            
            }

            

            
            //  console.log('arr',arr)
           
            if(this.state.num_Inputs>0){
              form.append(`properties`,JSON.stringify(arr))
            }
            
            //form.append('img',images);
            form.append('name', values.name);
            form.append('cashPrice', values.cashPrice);
            form.append('quantity', values.quantity);
            form.append('category', values.category.key);
            form.append('description', values.description);
            form.append('installmentPrice', values.installmentPrice);
            form.append('company', values.company);

           // console.log(form);

            let l = message.loading('Wait..', 2.5)
            axios.post(`${BASE_END_POINT}products`,form,{
              headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.props.currentUser.token}`
              },
            })
            .then(response=>{
                l.then(() => message.success('Add Product', 2.5))
                this.setState({ modal1Visible:false }); 
                this.getProducts(1,true)
                this.flag = -1
            })
            .catch(error=>{
                //console.log(error.response)
                l.then(() => message.error('Error', 2.5))
            })
  
          }
        });
        
      }
    //end submit form
      
//modal
    
    showModal = () => {
      this.setState({
        visible: true,
    });
      
    }

    
   
    
   

    setModal1Visible(modal1Visible) {
      this.setState({ modal1Visible });
    }

    
//end modal
    render() {
        //form
         const { getFieldDecorator } = this.props.form;
         //select
         const Option = Select.Option;
         const {num_Inputs} = this.state;

         
          let controls = (
            <Popconfirm
            title={allStrings.areusure}
            onConfirm={this.OKBUTTON}
            onCancel={this.fCANCELBUTTON}
            okText={allStrings.ok}
            cancelText={allStrings.cancel}
          >
             <Icon className='controller-icon' type="delete" />
          </Popconfirm>
         )

            let list =this.state.products.map(val=>[
              val.id,val.name,""+val.installmentPrice,""+val.quantity,""+val.description,""+val.company,""+val.category[0].categoryname,""+val.offer,""+val.top,""+val.hasOffer,controls
            ])
            const loadingView = [
              [<Skeleton  active/> ],
              [<Skeleton active/> ],
              [<Skeleton  active/> ],
              [<Skeleton active/> ],
              
             ]

      return (
          <div>
              <Menu></Menu>
              <Nav></Nav>
              <Tables
               columns={this.state.loading?['Loading...']:[allStrings.id,allStrings.name,allStrings.price,allStrings.count,allStrings.description,allStrings.company,allStrings.category,allStrings.offer,
                allStrings.top,allStrings.hasoffer,allStrings.remove]} 
              title={allStrings.producttable}
              onCellClick={(colData,cellMeta,)=>{
              //  console.log('col index  '+cellMeta.colIndex)
              //  console.log('row index   '+colData)
                if(cellMeta.colIndex==0){
                  
                 // console.log(this.state.products[cellMeta.rowIndex])
                  this.props.history.push('/ProductInfo',{data:this.state.products[this.pagentationPage+cellMeta.rowIndex]})
                }else if(cellMeta.colIndex==10){
                  const id = list[this.pagentationPage +cellMeta.rowIndex][0];
                  this.setState({selectedProduct:id})
                   // console.log(this.state.selectedProduct)
                   // console.log(id)
                  }
              }}
               onChangePage={(currentPage)=>{
                if(currentPage>this.counter){
                  this.counter=currentPage;
                  this.pagentationPage=this.pagentationPage+10
                }else{
                 this.counter=currentPage;
                 this.pagentationPage=this.pagentationPage-10
                }
                //console.log(currentPage)
                if(currentPage%2!=0  && currentPage > this.flag){
                  this.getProducts(currentPage+1)
                  this.flag  = currentPage;
                 
                }
                  
              }}
              arr={this.state.loading?loadingView:list}>
              </Tables>
              <div>
              <Button style={{color: 'white', backgroundColor:'#25272e', marginLeft:60}} onClick={() => this.setModal1Visible(true)}>{allStrings.addproduct}</Button>
              <Modal
                    title={allStrings.add}
                    okText={allStrings.ok}
                    cancelText={allStrings.cancel}
                    visible={this.state.modal1Visible}
                    onOk={this.handleSubmit}
                    onCancel={() => this.setModal1Visible(false)}
                  >
                    <Form onSubmit={this.handleSubmit} className="login-form">
                    
                    <Form.Item>
                    {getFieldDecorator('name', {
                        rules: [{ required: true, message: 'Please enter name' }],
                    })(
                        <Input placeholder={allStrings.name} />
                    )}
                    </Form.Item>
                    <Form.Item>
                    {getFieldDecorator('quantity', {
                        rules: [{ required: true, message: 'Please enter quantity' }],
                    })(
                        <Input  placeholder={allStrings.count} />
                    )}
                    </Form.Item>
                    <Form.Item>
                    {getFieldDecorator('description', {
                        rules: [{ required: true, message: 'Please enter description' }],
                    })(
                        <Input placeholder={allStrings.description} />
                    )}
                    </Form.Item>

                    <Form.Item>
                    {getFieldDecorator('company', {
                        rules: [{ required: true, message: 'Please enter description' }],
                    })(
                        <Input placeholder={allStrings.company} />
                    )}
                    </Form.Item>

                    <Form.Item>
                    {getFieldDecorator('category', {
                        rules: [{ required: true, message: 'Please enter category' }],
                    })(
                        <Select labelInValue  
                        placeholder={allStrings.type}
                        style={{ width: '100%'}} >
                        {this.state.categories.map(
                            val=><Option value={val.id}>{val.categoryname}</Option>
                        )}
                        </Select>
                    )}
                    </Form.Item>
                    
                   
                    
                    <Form.Item>
                    {getFieldDecorator('cashPrice', {
                        rules: [{ required: true, message: 'Please enter cash Price' }],
                    })(
                        <Input placeholder={allStrings.cachPrice}/>
                    )}
                    </Form.Item>
                    <Form.Item>
                    {getFieldDecorator('installmentPrice', {
                        rules: [{ required: true, message: 'Please enter installment Price' }],
                    })(
                        <Input placeholder={allStrings.installmentprice} />
                    )}
                    </Form.Item>
                    <Form.Item>
                    {getFieldDecorator('img', {
                        rules: [{ required: true, message: 'Please upload img' }],
                    })(
                        <input type="file" onChange= {this.onChange} multiple></input>
                    )}
                        
                    </Form.Item>
                    
                    {num_Inputs<10&&
                    <div style={{ padding:0,  display:'flex', width:'100%',flexDirection:this.props.isRTL?'row-reverse':'row',justifyContent:'space-between',alignItems:'center'}}>
                      <span style={{fontSize:20}}>{allStrings.addFiled}</span>
                      <Icon onClick={()=>this.setState({num_Inputs:num_Inputs+1})} className='controller-icon' type="plus" />
                    </div>
                    }

                    {num_Inputs>=1&&
                    <div style={{padding:0,  display:'flex', width:'100%',flexDirection:this.props.isRTL?'row-reverse':'row'}}>
                    <Form.Item>
                    {getFieldDecorator('key1', {
                        rules: [{ required: true, message: 'require' }],
                    })(
                        <Input style={{marginBottom:0, width:200,marginRight:10,marginLeft:10}} placeholder={allStrings.enterAttr} />
                    )}
                    </Form.Item>

                    <Form.Item>
                    {getFieldDecorator('value1', {
                        rules: [{ required: true, message: 'require' }],
                    })(
                        <Input style={{marginBottom:0,width:300,marginRight:15,marginLeft:15}}  placeholder={allStrings.enterVal} />
                    )}
                    </Form.Item>
                    </div>
                    }

                  {num_Inputs>=2&&
                    <div style={{padding:0,  display:'flex', width:'100%',flexDirection:this.props.isRTL?'row-reverse':'row'}}>
                    <Form.Item>
                    {getFieldDecorator('key2', {
                        rules: [{ required: true, message: 'require' }],
                    })(
                        <Input onChange={(e)=>{console.log("e  ",e.target.value)}} style={{marginBottom:0, width:200,marginRight:10,marginLeft:10}} placeholder={allStrings.enterAttr} />
                    )}
                    </Form.Item>

                    <Form.Item>
                    {getFieldDecorator('value2', {
                        rules: [{ required: true, message: 'require' }],
                        
                    })(
                        <Input style={{marginBottom:0,width:300,marginRight:15,marginLeft:15}}  placeholder={allStrings.enterVal} />
                    )}
                    </Form.Item>
                    </div>
                    }

                    {num_Inputs>=3&&
                    <div style={{padding:0,  display:'flex', width:'100%',flexDirection:this.props.isRTL?'row-reverse':'row'}}>
                    <Form.Item>
                    {getFieldDecorator('key3', {
                        rules: [{ required: true, message: 'require' }],
                    })(
                        <Input style={{marginBottom:0, width:200,marginRight:10,marginLeft:10}} placeholder={allStrings.enterAttr} />
                    )}
                    </Form.Item>

                    <Form.Item>
                    {getFieldDecorator('value3', {
                        rules: [{ required: true, message: 'require' }],
                    })(
                        <Input style={{marginBottom:0,width:300,marginRight:15,marginLeft:15}}  placeholder={allStrings.enterVal} />
                    )}
                    </Form.Item>
                    </div>
                    }

                    {num_Inputs>=4&&
                    <div style={{padding:0,  display:'flex', width:'100%',flexDirection:this.props.isRTL?'row-reverse':'row'}}>
                    <Form.Item>
                    {getFieldDecorator('key4', {
                        rules: [{ required: true, message: 'require' }],
                    })(
                        <Input style={{marginBottom:0, width:200,marginRight:10,marginLeft:10}} placeholder={allStrings.enterAttr} />
                    )}
                    </Form.Item>

                    <Form.Item>
                    {getFieldDecorator('value4', {
                        rules: [{ required: true, message: 'require' }],
                    })(
                        <Input style={{marginBottom:0,width:300,marginRight:15,marginLeft:15}}  placeholder={allStrings.enterVal} />
                    )}
                    </Form.Item>
                    </div>
                    }


                    {num_Inputs>=5&&
                    <div style={{padding:0,  display:'flex', width:'100%',flexDirection:this.props.isRTL?'row-reverse':'row'}}>
                    <Form.Item>
                    {getFieldDecorator('key5', {
                        rules: [{ required: true, message: 'require' }],
                    })(
                        <Input style={{marginBottom:0, width:200,marginRight:10,marginLeft:10}} placeholder={allStrings.enterAttr} />
                    )}
                    </Form.Item>

                    <Form.Item>
                    {getFieldDecorator('value5', {
                        rules: [{ required: true, message: 'require' }],
                    })(
                        <Input style={{marginBottom:0,width:300,marginRight:15,marginLeft:15}}  placeholder={allStrings.enterVal} />
                    )}
                    </Form.Item>
                    </div>
                    }  

                    {num_Inputs>=6&&
                    <div style={{padding:0,  display:'flex', width:'100%',flexDirection:this.props.isRTL?'row-reverse':'row'}}>
                    <Form.Item>
                    {getFieldDecorator('key6', {
                        rules: [{ required: true, message: 'require' }],
                    })(
                        <Input style={{marginBottom:0, width:200,marginRight:10,marginLeft:10}} placeholder={allStrings.enterAttr} />
                    )}
                    </Form.Item>

                    <Form.Item>
                    {getFieldDecorator('value6', {
                        rules: [{ required: true, message: 'require' }],
                    })(
                        <Input style={{marginBottom:0,width:300,marginRight:15,marginLeft:15}}  placeholder={allStrings.enterVal} />
                    )}
                    </Form.Item>
                    </div>
                    }  

                    {num_Inputs>=7&&
                    <div style={{padding:0,  display:'flex', width:'100%',flexDirection:this.props.isRTL?'row-reverse':'row'}}>
                    <Form.Item>
                    {getFieldDecorator('key7', {
                        rules: [{ required: true, message: 'require' }],
                    })(
                        <Input style={{marginBottom:0, width:200,marginRight:10,marginLeft:10}} placeholder={allStrings.enterAttr} />
                    )}
                    </Form.Item>

                    <Form.Item>
                    {getFieldDecorator('value7', {
                        rules: [{ required: true, message: 'require' }],
                    })(
                        <Input style={{marginBottom:0,width:300,marginRight:15,marginLeft:15}}  placeholder={allStrings.enterVal} />
                    )}
                    </Form.Item>
                    </div>
                    } 

                    {num_Inputs>=8&&
                    <div style={{padding:0,  display:'flex', width:'100%',flexDirection:this.props.isRTL?'row-reverse':'row'}}>
                    <Form.Item>
                    {getFieldDecorator('key8', {
                        rules: [{ required: true, message: 'require' }],
                    })(
                        <Input style={{marginBottom:0, width:200,marginRight:10,marginLeft:10}} placeholder={allStrings.enterAttr} />
                    )}
                    </Form.Item>

                    <Form.Item>
                    {getFieldDecorator('value8', {
                        rules: [{ required: true, message: 'require' }],
                    })(
                        <Input style={{marginBottom:0,width:300,marginRight:15,marginLeft:15}}  placeholder={allStrings.enterVal} />
                    )}
                    </Form.Item>
                    </div>
                    }   

                    {num_Inputs>=9&&
                    <div style={{padding:0,  display:'flex', width:'100%',flexDirection:this.props.isRTL?'row-reverse':'row'}}>
                    <Form.Item>
                    {getFieldDecorator('key9', {
                        rules: [{ required: true, message: 'require' }],
                    })(
                        <Input style={{marginBottom:0, width:200,marginRight:10,marginLeft:10}} placeholder={allStrings.enterAttr} />
                    )}
                    </Form.Item>

                    <Form.Item>
                    {getFieldDecorator('value9', {
                        rules: [{ required: true, message: 'require' }],
                    })(
                        <Input style={{marginBottom:0,width:300,marginRight:15,marginLeft:15}}  placeholder={allStrings.enterVal} />
                    )}
                    </Form.Item>
                    </div>
                    }  

                    {num_Inputs>=10&&
                    <div style={{padding:0,  display:'flex', width:'100%',flexDirection:this.props.isRTL?'row-reverse':'row'}}>
                    <Form.Item>
                    {getFieldDecorator('key10', {
                        rules: [{ required: true, message: 'require' }],
                    })(
                        <Input style={{marginBottom:0, width:200,marginRight:10,marginLeft:10}} placeholder={allStrings.enterAttr} />
                    )}
                    </Form.Item>

                    <Form.Item>
                    {getFieldDecorator('value10', {
                        rules: [{ required: true, message: 'require' }],
                    })(
                        <Input style={{marginBottom:0,width:300,marginRight:15,marginLeft:15}}  placeholder={allStrings.enterVal} />
                    )}
                    </Form.Item>
                    </div>
                    }  
                   
                   {num_Inputs>0&&
                   <div style={{padding:0, overflow: 'hidden' , width:'100%'}}>
                   <Icon onClick={()=>this.setState({num_Inputs:num_Inputs-1})} className='controller-icon' type="close" style={{float:this.props.isRTL?'left':'right'}} />
                   </div>
                   }
                   
                    </Form>
                </Modal>
                


              </div>  
              <Footer></Footer>
          </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,

  })
  
  const mapDispatchToProps = {
  }

  export default withRouter( connect(mapToStateProps,mapDispatchToProps) ( Product = Form.create({ name: 'normal_login' })(Product) ) );
  