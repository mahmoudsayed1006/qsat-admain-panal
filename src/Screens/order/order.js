import React from 'react';
import Menu from '../../components/menu/menu';
import Nav from '../../components/navbar/navbar';
import Tables from '../../components/table/table';
import Footer from '../../components/footer/footer';

import './order.css';
import {Skeleton, Icon,Popconfirm, message} from 'antd';
import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import {withRouter} from 'react-router-dom'
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'

class Pending extends React.Component {
  pagentationPage=0;
    counter=0;
    state = {
        visible: false,
        confirmDelete: false,
        selectedOrder:null,
        visible: false,
        orders:[],
        loading:true
        }

        constructor(props){
          super(props)
          if(this.props.isRTL){
            allStrings.setLanguage('ar')
          }else{
            allStrings.setLanguage('en')
          }
        }
        
       //submit form
       //PENDING
       flag = -1;
       getOrders= (page,deleteRow) => {
         axios.get(`${BASE_END_POINT}orders?status=PENDING&page=${page}&limit={20}`)
         .then(response=>{
           //console.log("ALL orders")
           //console.log(response.data.data)
           //console.log(response.data.data[0].productOrders)
           this.setState({orders:deleteRow?response.data.data:[...this.state.orders,...response.data.data],loading:false})
         })
         .catch(error=>{
           //console.log("ALL orders ERROR")
           //console.log(error.response)
           this.setState({loading:false})
         })
       }
       componentDidMount(){
         //console.log(this.props.currentUser)
         this.getOrders(1)
       }
       OKBUTTON = (e) => {
        this.deleteOrder()
       }
 
       deleteOrder = () => {
         let l = message.loading('Wait..', 2.5)
         axios.delete(`${BASE_END_POINT}orders/${this.state.selectedOrder}`,{
           headers: {
             'Content-Type': 'application/x-www-form-urlencoded',
             'Authorization': `Bearer ${this.props.currentUser.token}`
           },
         })
         .then(response=>{
             l.then(() => message.success('Order Deleted', 2.5))
             this.getOrders(1,true)
                this.flag = -1
         })
         .catch(error=>{
             //console.log(error.response)
             l.then(() => message.error('Error', 2.5))
         })
      }

    render() {
 
      let controls = (
        <Popconfirm
        title="Are you sure delete this item?"
        onConfirm={this.OKBUTTON}
        onCancel={this.fCANCELBUTTON}
        okText="Yes"
        cancelText="No"
      >
         <Icon className='controller-icon' type="delete" />
      </Popconfirm>
     )
  let list = this.state.orders.map((val,index)=>{
        
    return  [
    val.id,val.client.firstname+" "+val.client.lastname ,"mohamed",
    val.productOrders.map(v=><h6>{v.product.name}</h6>),
    val.productOrders.map(v=><h6>{v.count}</h6>),
    val.total,
    val.productOrders.map(v=><h6>{v.firstPaid}</h6>),
    val.productOrders.map(v=><h6>{v.monthCount}</h6>),
    val.productOrders.map(v=><h6>{v.costPerMonth}</h6>),
    "ismailia",
    val.createdAt.substring(0, 10)
    ]
  })    

  list.forEach(function(row) {
    row.push(controls)
   });

   const loadingView = [
    [<Skeleton  active/> ],
    [<Skeleton active/> ],
    [<Skeleton  active/> ],
    [<Skeleton active/> ],
    
   ]
 
      return (
        <div>
        <Menu></Menu>
        <Nav></Nav>
        <Tables columns={this.state.loading?['wait']:[allStrings.id,allStrings.client,allStrings.salesman,allStrings.product,allStrings.count,allStrings.total,allStrings.firstpaid,allStrings.monthcount,allStrings.costPerMonth,allStrings.Destination,allStrings.date,allStrings.remove]} 
        title={allStrings.pendingtable}
        onCellClick={(colData,cellMeta,)=>{
         // console.log('col index  '+cellMeta.colIndex)
         // console.log('row index   '+colData)
          if(cellMeta.colIndex==0){
            
           // console.log(this.state.orders[cellMeta.rowIndex])
            this.props.history.push('/OrderInfo',{data:this.state.orders[this.pagentationPage+cellMeta.rowIndex]})
          }else if(cellMeta.colIndex==11){
            const id = list[this.pagentationPage +cellMeta.rowIndex][0];
            this.setState({selectedOrder:id})
             // console.log(id)
            }
        }}
         onChangePage={(currentPage)=>{
          if(currentPage>this.counter){
            this.counter=currentPage;
            this.pagentationPage=this.pagentationPage+10
          }else{
           this.counter=currentPage;
           this.pagentationPage=this.pagentationPage-10
          }
                //console.log(currentPage)
                if(currentPage%2!=0  && currentPage > this.flag){
                  this.getOrders(currentPage+1)
                  this.flag  = currentPage;
                 
                }
                  
              }}
        arr={this.state.loading?loadingView:list}
       
        ></Tables>
        <Footer></Footer>
    </div>
      );
    }
  }

  
  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,

  })
  
  const mapDispatchToProps = {
  }


export default withRouter(connect(mapToStateProps,mapDispatchToProps) (Pending) );
