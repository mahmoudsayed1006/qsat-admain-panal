import React from 'react';
import Menu from '../../components/menu/menu';
import Nav from '../../components/navbar/navbar';
import Footer from '../../components/footer/footer';

import './partners info.css';
import "antd/dist/antd.css";
 import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux';
import { Upload, message,Modal, Form, Icon, Input, Button,Select} from 'antd';
import  {allStrings} from '../../assets/strings'


class PartnersInfo extends React.Component {
    state = {
        modal1Visible: false,
        modal2Visible: false,
        partners:this.props.location.state.data
         }

         constructor(props){
          super(props)
          if(this.props.isRTL){
            allStrings.setLanguage('ar')
          }else{
            allStrings.setLanguage('en')
          }
        }
        
        
    
    deletePartners = () => {
        let l = message.loading('Wait..', 2.5)
        axios.delete(`${BASE_END_POINT}partners/${this.state.partners.id}`,{
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        })
        .then(response=>{
            l.then(() => message.success('partner Deleted', 2.5));
            this.props.history.goBack()

        })
        .catch(error=>{
            //console.log(error.response)
            l.then(() => message.error('Error', 2.5))
        })
     }
     finishPartners = (profit) => {
      
      let l = message.loading('Wait..', 2.5)
       axios.put(`${BASE_END_POINT}partners/${this.state.partners.id}/finish`,JSON.stringify({
        profit:profit
      }),{
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${this.props.currentUser.token}`
        },
      })
       .then(response=>{
          //  console.log('done')
            l.then(() => message.success('partner finish', 2.5));
            this.setState({ modal2Visible:false });
            this.props.history.goBack()
       })
       .catch(error=>{
       // console.log('Error')
       // console.log(error.response)
        l.then(() => message.error('Error', 2.5))
       })
      }

      unfinishPartners = () => {
        
          let l = message.loading('Wait..', 2.5)
          axios.put(`${BASE_END_POINT}partners/${this.state.partners.id}/unfinish`,{},{
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${this.props.currentUser.token}`
            },
          })
          .then(response=>{
               // console.log('done')
                l.then(() => message.success('partner unfinish', 2.5))
                this.props.history.goBack()
          })
          .catch(error=>{
            //console.log('Error')
           // console.log(error.response)
            l.then(() => message.error('Error', 2.5))
          })
      }
      handleProfitSubmit = (e) => {
        e.preventDefault();
       
        this.props.form.validateFieldsAndScroll(['profit'],(err, values) => {
          if (!err) {
           // console.log('Received values of form: ', values);
            this.finishPartners(values.profit)
        }
        });
      
      }
     handleSubmit = (e) => {
      e.preventDefault();
      this.props.form.validateFieldsAndScroll(['name','cost','percentage'],(err, values) => {
        if (!err) {
         // console.log('Received values of form: ', values);
          let l = message.loading('Wait..', 2.5)
          axios.put(`${BASE_END_POINT}partners/${this.state.partners.id}`,JSON.stringify(values),{
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${this.props.currentUser.token}`
            },
          })
          .then(response=>{
              l.then(() => message.success('Partner Updated', 2.5));
              this.setState({ modal1Visible:false });
              this.props.history.goBack()
          })
          .catch(error=>{
             // console.log(error.response)
              l.then(() => message.error('Error', 2.5))
          })
        }
      });
      
    }

      
      //modal
     
      setModal1Visible(modal1Visible) {
        this.setState({ modal1Visible });
      }
      setModal2Visible(modal2Visible) {
        this.setState({ modal2Visible });
      }
    render() {
        const { getFieldDecorator } = this.props.form;
         //select
         const Option = Select.Option;
         const {partners} = this.state

        
         //end select
        //upload props
        const props = {
          name: 'file',
          action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
          headers: {
            authorization: 'authorization-text',
          },
          onChange(info) {
            if (info.file.status !== 'uploading') {
             // console.log(info.file, info.fileList);
            }
            if (info.file.status === 'done') {
              message.success(`${info.file.name} file uploaded successfully`);
            } else if (info.file.status === 'error') {
              message.error(`${info.file.name} file upload failed.`);
            }
          },
        };

        //end upload props
      return (
          
        <div>
         <Menu></Menu>
              <Nav></Nav>
        <div className='login'>
        <div class="row">
            <div class="col m2">
                <div class='title' style={{backgroundColor:'#25272e'}} >
                    <h2 class="center-align" style={{color:'#fff'}}>{allStrings.partnerinfo}</h2>
                </div>

                <div class="row">
                    <form class="col s12">
                    <div class="row">
                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={partners.name}>
                            </input>
                            <label for="name" class="active">{allStrings.name}</label>
                            </div>
                            <div class="input-field col s6">
                            <input id="status" type="text" class="validate" disabled value={""+partners.status}></input>
                            <label for="status" class="active">{allStrings.status}</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="cost" type="text" class="validate" disabled disabled value={partners.cost}>
                            </input>
                            <label for="cost" class="active">{allStrings.cost}</label>
                            </div>
                            <div class="input-field col s6">
                            <input id="percentage" type="text" class="validate" disabled disabled value={"%"+partners.percentage}>
                            </input>
                            <label for="percentage" class="active">{allStrings.percentage}</label>
                            </div>
                            
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="profit" type="text" class="validate" disabled disabled value={partners.profit}>
                            </input>
                            <label for="profit" class="active">{allStrings.profit}</label>
                            </div>
                            
                        </div>
                        
                       
                            <a class="waves-effect waves-light btn btn-large delete " onClick={this.deletePartners}><i class="material-icons left spcial">delete</i>{allStrings.remove}</a>
                            <a class="waves-effect waves-light btn btn-large edit" onClick={() => this.setModal1Visible(true)}><i class="material-icons left spcial">edit</i>{allStrings.edit}</a>
                            <a class="waves-effect waves-light btn btn-large" onClick={() => this.setModal2Visible(true)}><i class="material-icons left spcial">done</i>{allStrings.finish}</a>
                            <a class="waves-effect waves-light btn btn-large warning" onClick={this.unfinishPartners}><i class="material-icons left spcial">close</i>{allStrings.unfinish}</a>


                            <div>
                            <Modal
                                title={allStrings.edit}
                                visible={this.state.modal1Visible}
                                onOk={this.handleSubmit}
                                onCancel={() => this.setModal1Visible(false)}
                                okText={allStrings.ok}
                                cancelText={allStrings.cancel}
                              >
                               <Form onSubmit={this.handleSubmit} className="login-form">
                                  <Form.Item>
                                  {getFieldDecorator('name', {
                                      rules: [{ required: true, message: 'Please enter name' }],
                                      initialValue: partners.name
                                  })(
                                      <Input placeholder={allStrings.name} />
                                  )}
                                  </Form.Item>
                                  <Form.Item>
                                  {getFieldDecorator('cost', {
                                      rules: [{ required: true, message: 'Please enter name' }],
                                      initialValue: partners.cost
                                  })(
                                      <Input placeholder={allStrings.cost} />
                                  )}
                                  </Form.Item>
                                  <Form.Item>
                                  {getFieldDecorator('percentage', {
                                      rules: [{ required: true, message: 'Please enter percentage' }],
                                      initialValue: partners.percentage
                                  })(
                                      <Input placeholder={allStrings.percentage} />
                                  )}
                                  </Form.Item>
                                 
                                  
                              </Form>
                            </Modal>
                            <Modal
                                title={allStrings.finish}
                                visible={this.state.modal2Visible}
                                onOk={this.handleProfitSubmit}
                                onCancel={() => this.setModal2Visible(false)}
                                okText={allStrings.ok}
                                cancelText={allStrings.cancel}
                              >
                              <Form onSubmit={this.handleProfitSubmit} className="login-form">
                                  <Form.Item>
                                  {getFieldDecorator('profit', {
                                      rules: [{ required: true, message: 'Please enter profit' }],
                                      
                                  })(
                                      <Input placeholder={allStrings.profit} />
                                  )}
                                 </Form.Item>
                                  
                              </Form>
                            </Modal>

                                
                        
                            </div>
                        </form>
                        
                    </div>
            </div>
        </div>
        </div>
        <Footer></Footer>
    </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,

  })
  
  const mapDispatchToProps = {
  }

export default connect(mapToStateProps,mapDispatchToProps) (PartnersInfo = Form.create({ name: 'normal_login', })(PartnersInfo)) ;
