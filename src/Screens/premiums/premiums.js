
import React, { Component } from 'react';
import Menu from '../../components/menu/menu';
import Nav from '../../components/navbar/navbar';
import Tables from '../../components/table/table';
import Footer from '../../components/footer/footer';

import './premiums.css';
//import {Icon,Button,Modal,Input} from 'react-materialize';
import { Skeleton,Modal, Form, Icon, Input, Button,Select,DatePicker,Popconfirm, message} from 'antd';
import "antd/dist/antd.css";
import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'



class Premiums extends React.Component {

    constructor(props){
        super(props)
        if(this.props.isRTL){
          allStrings.setLanguage('ar')
        }else{
          allStrings.setLanguage('en')
        }
        this.getpremiums(1)
      }
    pagentationPage=0;
    counter=0;
    state = {
        visible: false,
        confirmDelete: false,
        selectedPremiums:null,
        premiums:[],
        loading:true
        }
       //submit form
       flage=-1;
       getpremiums = (page,deleteRow) => {
         axios.get(`${BASE_END_POINT}premiums?page=${page}&limit={20}`)
         .then(response=>{
          // console.log("ALL premiums")
          // console.log(response.data)
           this.setState({premiums:deleteRow?response.data.data:[...this.state.premiums,...response.data.data],loading:false})
         })
         .catch(error=>{
         //  console.log("ALL premiums ERROR")
           console.log(error.response)
         })
       }
       
       OKBUTTON = (e) => {
        this.deletePremiums()
       }
 
       deletePremiums = () => {
         let l = message.loading('Wait..', 2.5)
         axios.delete(`${BASE_END_POINT}premiums/${this.state.selectedPremiums}`,{
           headers: {
             'Content-Type': 'application/x-www-form-urlencoded',
             'Authorization': `Bearer ${this.props.currentUser.token}`
           },
         })
         .then(response=>{
             l.then(() => message.success('Premiums Deleted', 2.5))
             this.getpremiums(1,true)
             this.flag = -1
         })
         .catch(error=>{
            // console.log(error.response)
             l.then(() => message.error('Error', 2.5))
         })
      }
      sendNotif = () => {
        let l = message.loading('Wait..', 2.5)
        axios.post(`${BASE_END_POINT}premiums/SendRememberNotif`,{},{
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        })
        .then(response=>{
            l.then(() => message.success('remember notif send', 2.5))
        })
        .catch(error=>{
           // console.log(error.response)
            l.then(() => message.error('Error', 2.5))
        })
     }
       

    //submit form
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
          if (!err) {
           // console.log('Received values of form: ', values);
           /* let l = message.loading('Wait..', 2.5)
        axios.post(`${BASE_END_POINT}partners`,JSON.stringify(values),{
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxIiwiaXNzIjoiYWtmYSIsImlhdCI6MTU1NjU0ODAwNTMyNCwiZXhwIjoxNTU2NTUwNTk3MzI0fQ.Fj67rxFtrO5dfJd9Jv-VNMWPs2oR9KT91kfycVgmVLI`
          },
        })
        .then(response=>{
            l.then(() => message.success('Add Partner', 2.5));
            this.setState({ modal1Visible:false });
        })
        .catch(error=>{
            console.log(error.response)
            l.then(() => message.error('Error', 2.5))
        })*/
          }
        });
        
      }
      
    //end submit form
      
//modal
   

      setModal1Visible(modal1Visible) {
        this.setState({ modal1Visible });
      }

//end modal
    render() {
       
         //end select
         let controls = (
            <Popconfirm
            title={allStrings.areusure}
            onConfirm={this.OKBUTTON}
            onCancel={this.fCANCELBUTTON}
            okText={allStrings.ok}
            cancelText={allStrings.cancel}
          >
             <Icon className='controller-icon' type="delete" />
          </Popconfirm>
         )
         
         let list =this.state.premiums.map(val=>[
            val.id,val.product.name,""+val.client.firstname+" "+val.client.lastname,
            ""+val.salesMan.firstname+" "+val.salesMan.lastname,""+val.branch.branchName,val.totalMonth,val.costPerMonth,
            val.status,val.paidDate.substring(0, 10),
        ])

         list.forEach(function(row) {
            row.push(controls)
           });
           const loadingView = [
            [<Skeleton  active/> ],
            [<Skeleton active/> ],
            [<Skeleton  active/> ],
            [<Skeleton active/> ],
            
           ]
      return (
          <div>
              <Menu></Menu>
              <Nav></Nav>
              <Tables
              columns={this.state.loading?['Loading...']:[allStrings.id,allStrings.product, allStrings.client,allStrings.salesman,allStrings.branches,allStrings.totalMonth,allStrings.costPerMonth,allStrings.status,allStrings.paidDate,allStrings.remove]} 
              title={allStrings.premiumsTable} 
              onCellClick={(colData,cellMeta,)=>{
               // console.log('col index  '+cellMeta.colIndex)
               // console.log('row index   '+colData)
                if(cellMeta.colIndex==0){
                  
                  console.log(this.state.premiums[cellMeta.rowIndex])
                  this.props.history.push('/PremiumsInfo',{data:this.state.premiums[this.pagentationPage+cellMeta.rowIndex]})
                }else if(cellMeta.colIndex==8){
                  const id = list[this.pagentationPage +cellMeta.rowIndex][0];
                  this.setState({selectedPremiums:id})
                    //console.log(id)
                  }
              }}
               onChangePage={(currentPage)=>{
                if(currentPage>this.counter){
                  this.counter=currentPage;
                  this.pagentationPage=this.pagentationPage+10
                }else{
                 this.counter=currentPage;
                 this.pagentationPage=this.pagentationPage-10
                }
              //  console.log(currentPage)
                if(currentPage%2!=0  && currentPage > this.flag){
                  this.getpremiums(currentPage+1)
                  this.flag  = currentPage;
                 
                }
                  
              }}
              arr={this.state.loading?loadingView:list}>
              </Tables>
              <Button style={{height:50, color: 'white', backgroundColor:'#25272e', marginLeft:60}} onClick={() => this.sendNotif()}>{allStrings.rememberpepole}</Button>
                <br></br>
                <br></br>
              <div>
              {/*<Button type="primary" onClick={() => this.setModal1Visible(true)}>Add Premiums</Button>
              <Modal
                    title="Add"
                    visible={this.state.modal1Visible}
                    onOk={this.handleSubmit}
                    onCancel={() => this.setModal1Visible(false)}
                  >
                    <Form onSubmit={this.handleSubmit} className="login-form">
                    <Form.Item>
                    {getFieldDecorator('client', {
                        rules: [{ required: true, message: 'Please enter client' }],
                    })(
                        <Select labelInValue defaultValue={{ key: '1' }} 
                        placeholder="Select client"
                        style={{ width: '100%'}} onChange={handleChange}>
                            <Option value="1">mohamed</Option>
                            <Option value="2">mahmoud</Option>
                        </Select>
                    )}
                    </Form.Item>
                    <Form.Item>
                    {getFieldDecorator('product', {
                        rules: [{ required: true, message: 'Please enter product' }],
                    })(
                        <Select labelInValue defaultValue={{ key: '1' }} 
                        placeholder="Select product"
                        style={{ width: '100%'}} onChange={handleChange}>
                            <Option value="1">Low</Option>
                            <Option value="2">Top</Option>
                        </Select>
                    )}
                    </Form.Item>
                    <Form.Item>
                    {getFieldDecorator('salesMan', {
                        rules: [{ required: true, message: 'Please enter sales Man' }],
                    })(
                        <Select labelInValue defaultValue={{ key: '1' }} 
                        placeholder="Select sales Man"
                        style={{ width: '100%'}} onChange={handleChange}>
                            <Option value="1">mohamed</Option>
                            <Option value="2">mahmoud</Option>
                        </Select>
                    )}
                    </Form.Item>
                    <Form.Item>
                    {getFieldDecorator('paidDate', {
                        rules: [{ required: true, message: 'Please enter paidDate' }],
                    })(
                      <DatePicker style={{ width: '100%'}}/>
                    )}
                    </Form.Item>
                    <Form.Item>
                    {getFieldDecorator('totalMonth', {
                        rules: [{ required: true, message: 'Please enter totalMonth' }],
                    })(
                        <Input placeholder="totalMonth" />
                    )}
                    </Form.Item>
                    <Form.Item>
                    {getFieldDecorator('costPerMonth', {
                        rules: [{ required: true, message: 'Please enter costPerMonth' }],
                    })(
                        <Input placeholder="costPerMonth" />
                    )}
                    </Form.Item>
                    <Form.Item>
                    {getFieldDecorator('monthRemaining', {
                        rules: [{ required: true, message: 'Please enter month Remaining' }],
                    })(
                        <Input placeholder="month Remaining" />
                    )}
                    </Form.Item>
                    <Form.Item>
                    {getFieldDecorator('costRemaining', {
                        rules: [{ required: true, message: 'Please enter cost Remaining' }],
                    })(
                        <Input placeholder="cost Remaining" />
                    )}
                    </Form.Item>
                    <Form.Item>
                    {getFieldDecorator('paidCost', {
                        rules: [{ required: true, message: 'Please enter paidCost' }],
                    })(
                        <Input placeholder="paidCost" />
                    )}
                    </Form.Item>
                    <Form.Item>
                    {getFieldDecorator('paidMonth', {
                        rules: [{ required: true, message: 'Please enter paidMonth' }],
                    })(
                        <Input placeholder="paidMonth" />
                    )}
                    </Form.Item>
                    </Form>
                </Modal>
               {/*} <Modal
                  title="Edit"
                  visible={this.state.modal2Visible}
                  onOk={this.handleSubmit2}
                  onCancel={() => this.setModal2Visible(false)}
                >
                  <Form onSubmit={this.handleSubmit2} className="login-form">
                  <Form.Item>
                    {getFieldDecorator('client', {
                        rules: [{ required: true, message: 'Please enter client' }],
                    })(
                        <Select labelInValue defaultValue={{ key: '1' }} 
                        placeholder="Select client"
                        style={{ width: '100%'}} onChange={handleChange}>
                            <Option value="1">mohamed</Option>
                            <Option value="2">mahmoud</Option>
                        </Select>
                    )}
                    </Form.Item>
                    <Form.Item>
                    {getFieldDecorator('product', {
                        rules: [{ required: true, message: 'Please enter product' }],
                    })(
                        <Select labelInValue defaultValue={{ key: '1' }} 
                        placeholder="Select product"
                        style={{ width: '100%'}} onChange={handleChange}>
                            <Option value="1">Low</Option>
                            <Option value="2">Top</Option>
                        </Select>
                    )}
                    </Form.Item>
                    <Form.Item>
                    {getFieldDecorator('salesMan', {
                        rules: [{ required: true, message: 'Please enter sales Man' }],
                    })(
                        <Select labelInValue defaultValue={{ key: '1' }} 
                        placeholder="Select sales Man"
                        style={{ width: '100%'}} onChange={handleChange}>
                            <Option value="1">mohamed</Option>
                            <Option value="2">mahmoud</Option>
                        </Select>
                    )}
                    </Form.Item>
                    <Form.Item>
                    {getFieldDecorator('paidDate', {
                        rules: [{ required: true, message: 'Please enter paidDate' }],
                    })(
                      <DatePicker style={{ width: '100%'}}/>
                    )}
                    </Form.Item>
                    <Form.Item>
                    {getFieldDecorator('totalMonth', {
                        rules: [{ required: true, message: 'Please enter totalMonth' }],
                    })(
                        <Input placeholder="totalMonth" />
                    )}
                    </Form.Item>
                    <Form.Item>
                    {getFieldDecorator('costPerMonth', {
                        rules: [{ required: true, message: 'Please enter costPerMonth' }],
                    })(
                        <Input placeholder="costPerMonth" />
                    )}
                    </Form.Item>
                    <Form.Item>
                    {getFieldDecorator('monthRemaining', {
                        rules: [{ required: true, message: 'Please enter month Remaining' }],
                    })(
                        <Input placeholder="month Remaining" />
                    )}
                    </Form.Item>
                    <Form.Item>
                    {getFieldDecorator('costRemaining', {
                        rules: [{ required: true, message: 'Please enter cost Remaining' }],
                    })(
                        <Input placeholder="cost Remaining" />
                    )}
                    </Form.Item>
                    <Form.Item>
                    {getFieldDecorator('paidCost', {
                        rules: [{ required: true, message: 'Please enter paidCost' }],
                    })(
                        <Input placeholder="paidCost" />
                    )}
                    </Form.Item>
                    <Form.Item>
                    {getFieldDecorator('paidMonth', {
                        rules: [{ required: true, message: 'Please enter paidMonth' }],
                    })(
                        <Input placeholder="paidMonth" />
                    )}
                    </Form.Item>
                  </Form>
                    </Modal>{*/}
                    
              </div>
             <Footer></Footer>
          </div>
      );
    }
  }
  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,

  })
  
  const mapDispatchToProps = {
  }

  export default connect(mapToStateProps,mapDispatchToProps) ( Premiums = Form.create({ name: 'normal_login' })(Premiums));

