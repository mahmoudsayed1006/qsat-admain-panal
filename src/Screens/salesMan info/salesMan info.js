import React from 'react';
import Menu from '../../components/menu/menu';
import Nav from '../../components/navbar/navbar';
import Footer from '../../components/footer/footer';

import './salesMan info.css';
import {Input} from 'react-materialize';
import "antd/dist/antd.css";
 import { Modal,Form ,Select ,message} from 'antd';
 import axios from 'axios';
 import {BASE_END_POINT} from '../../config/URL'
import {Table} from 'react-materialize'
import { connect } from 'react-redux'
import  {allStrings} from '../../assets/strings'



class SalesManInfo extends React.Component {
      state = {
        modal1Visible: false,
        salesMan:this.props.location.state.data,
        file:this.props.location.state.data.img,
        clients:[],
      }

      constructor(props){
        super(props)
        if(this.props.isRTL){
          allStrings.setLanguage('ar')
        }else{
          allStrings.setLanguage('en')
        }

        //console.log("Data    ",this.props.location.state.data)
        this.getClients()
      }
 
      onChange = (e) => {
        this.setState({file:e.target.files[0]});
    }

    getClients = () => {
        axios.get(`${BASE_END_POINT}premiums/AllClients?salesMan=${this.props.location.state.data.id}`,{
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        })
        .then(response=>{
            //console.log("get client   ",response.data.data)
            const f = response.data.data
            let friends = []
            let ids = []
            for (var i = 0; i < f.length; i++) {
            let user = f[i]
            //console.log("MSG Friend  ",user)
            if (!ids.includes(user.client.id)) {
                friends.push(user)
                ids.push(user.client.id)
            }
            }
           this.setState({clients:friends})
        })
        .catch(error=>{
            console.log("get client error   ",error.response)
           
        })
     }

    activeUser = (active) => {
        let uri ='';
        if(active){
         uri = `${BASE_END_POINT}${this.state.salesMan.id}/active`
        }else{
         uri = `${BASE_END_POINT}${this.state.salesMan.id}/disactive`
        }
       let l = message
        .loading('Wait..', 2.5)
        axios.put(uri,{},{
         headers: {
           'Content-Type': 'application/json',
           'Authorization': `Bearer ${this.props.currentUser.token}`
         },
       })
        .then(response=>{
            // console.log('done')
             if(active){
                 
                 l.then(() => message.success('User Activated', 2.5))
                 
             }else{
             
                l.then(() => message.success('User DisActivated', 2.5))
             }
             this.props.history.goBack()
        })
        .catch(error=>{
        // console.log('Error')
         //console.log(error.response)
         l.then(() => message.error('Error', 2.5))
        })
    }
       //submit form
       handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
          if (!err) {
           // console.log('Received values of form: ', values);
            var form = new FormData();
            if(this.state.file){
                form.append('img',this.state.file);
            }
            form.append('firstname', values.firstname);
            form.append('lastname', values.lastname);
            form.append('email', values.email);
            form.append('phone', values.phone);
            form.append('address', values.address);
            form.append('cardNum', values.cardNum);
            form.append('area', values.area);
            form.append('job', values.job);
            form.append('jobLocation', values.jobLocation);
            form.append('type', values.type.key);
            let l = message.loading('Wait..', 2.5)
            axios.put(`${BASE_END_POINT}user/${this.state.salesMan.id}/updateInfo`,form,{
              headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.props.currentUser.token}`
              },
            })
            .then(response=>{
                l.then(() => message.success('SalesMan Updated', 2.5));
                this.setState({ modal1Visible:false });
                this.props.history.goBack()
            })
            .catch(error=>{
             //   console.log(error.response)
                l.then(() => message.error('Error', 2.5))
            })

          }
        });
        
      }

    //end submit form
      
      //modal
     
  
      setModal1Visible(modal1Visible) {
        this.setState({ modal1Visible });
      }
  

  //end modal
  
    render() {
        const {salesMan} = this.state
        const { getFieldDecorator } = this.props.form;
         //select
         const Option = Select.Option;

         function handleChange(value) {
            //console.log(value); 
         }
         //end select
       
      return (
          
        <div>
         <Menu></Menu>
              <Nav></Nav>
        <div className='login'>
        <div class="row">
            <div class="col m2">
                <div class='title' style={{backgroundColor:'#25272e'}} >
                    <h2 class="center-align" style={{color:'#fff'}}>{allStrings.salesman}</h2>
                </div>
                <div class="row">
                
                    <form class="col s12">
                    <img style={{borderColor:'#25272e'}} src={salesMan.img?salesMan.img:"https://theimag.org/wp-content/uploads/2015/01/user-icon-png-person-user-profile-icon-20.png"}></img>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="firstname" type="text" class="validate" disabled value={salesMan.firstname}>
                            </input>
                            <label for="firstname" class="active">{allStrings.firstname}</label>
                            </div>
                            <div class="input-field col s6">
                            <input id="lastname" type="text" class="validate" disabled value={salesMan.lastname}></input>
                            <label for="lastname" class="active">{allStrings.lastname}</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="email" type="text" class="validate" disabled value={salesMan.email}>
                            </input>
                            <label for="email" class="active">{allStrings.email}</label>
                            </div>
                            <div class="input-field col s6">
                            <input id="phone" type="text" class="validate" disabled value={salesMan.phone}></input>
                            <label for="phone" class="active">{allStrings.phone}</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="type" type="text" class="validate" disabled value={salesMan.type}>
                            </input>
                            <label for="type" class="active">{allStrings.type}</label>
                            </div>
                            <div class="input-field col s6">
                            <input id="are" type="text" class="validate" disabled value={salesMan.area}></input>
                            <label for="area" class="active">{allStrings.area}</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="cardNum" type="text" class="validate" disabled value={salesMan.cardNum}>
                            </input>
                            <label for="cardNum" class="active">{allStrings.cardnumber}</label>
                            </div>
                            <div class="input-field col s6">
                            <input id="Address" type="text" class="validate" disabled value={salesMan.address}></input>
                            <label for="Address" class="active">{allStrings.address}</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="Job" type="text" class="validate" disabled value={salesMan.job}>
                            </input>
                            <label for="Job" class="active">{allStrings.job}</label>
                            </div>
                            <div class="input-field col s6">
                            <input id="job Location" type="text" class="validate" disabled value={salesMan.jobLocation}></input>
                            <label for="job Location" class="active">{allStrings.jobLocation}</label>
                            </div>
                        </div>


                        <div className='dash-table'>
                            <h5>{allStrings.client} :</h5>
                            <div className='row'>
                                <div className="col s6 m6 l6 dashboard-table">
                                    <Table>
                                        <thead>
                                            <tr>
                                            <th data-field="id">{allStrings.id}</th>
                                            <th data-field="name">{allStrings.name}</th>
                                            <th data-field="email">{allStrings.email}</th>
                                            <th data-field="mobile">{allStrings.phone}</th>
                                            <th data-field="address">{allStrings.address}</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                        
                                                {this.state.clients.map(val=>(
                                                    <tr>
                                                     <td>{val.client.id}</td>
                                                     <td>{val.client.firstname+  val.client.lastname}</td>
                                                     <td>{val.client.email}</td>
                                                     <td>{val.client.phone}</td>
                                                     <td>{val.client.address}</td>
                                                     </tr>
                                                ))}
                                                
                                        </tbody>
                                    </Table>
                                </div>
                            </div>
                        </div>
                        
                        <a class="waves-effect waves-light btn btn-large delete"  onClick={()=>this.activeUser(false)} ><i class="spcial material-icons left">close</i>{allStrings.disactive}</a>
                            <a class="waves-effect waves-light btn btn-large"  onClick={()=>this.activeUser(true)}><i class=" spcial material-icons left">done</i>{allStrings.active}</a>
                            <a class="waves-effect waves-light btn btn-large edit" onClick={() => this.setModal1Visible(true)}><i class=" spcial material-icons left">edit</i>{allStrings.edit}</a>
                        </form>
                    <Modal
                            title="Edit"
                            visible={this.state.modal1Visible}
                            onOk={this.handleSubmit}
                            onCancel={() => this.setModal1Visible(false)}
                        >
                        <input type="file" onChange= {this.onChange}></input>
                            <Form onSubmit={this.handleSubmit} className="login-form">
                            <Form.Item>
                            {getFieldDecorator('firstname', {
                                rules: [{ required: true, message: 'Please enter First Name' }],
                                initialValue:salesMan.firstname
                            })(
                                <Input placeholder={allStrings.firstname} />
                            )}
                            </Form.Item>
                            <Form.Item>
                            {getFieldDecorator('lastname', {
                                rules: [{ required: true, message: 'Please enter last name' }],
                                initialValue:salesMan.lastname
                            })(
                                <Input placeholder={allStrings.lastname} />
                            )}
                            </Form.Item>
                        
                            <Form.Item>
                            {getFieldDecorator('email', {
                                rules: [{ required: true, message: 'Please enter email' }],
                                initialValue:salesMan.email
                            })(
                                <Input placeholder={allStrings.email} />
                            )}
                            </Form.Item>
                            <Form.Item>
                            {getFieldDecorator('phone', {
                                rules: [{ required: true, message: 'Please enter phone' }],
                                initialValue:salesMan.phone[0]
                            })(
                                <Input placeholder={allStrings.phone} />
                            )}
                            </Form.Item>
                            <Form.Item>
                            {getFieldDecorator('type', {
                                rules: [{ required: true, message: 'Please enter type' }],
                                //initialValue:salesMan.firstname
                            })(
                                <Select labelInValue  
                                placeholder={allStrings.type}
                                style={{ width: '100%'}} onChange={handleChange}>
                                    <Option value="SALES-MAN">{allStrings.salesman}</Option>
                                    
                                </Select>
                            )}
                            </Form.Item>
                            <Form.Item>
                            {getFieldDecorator('cardNum', {
                                rules: [{ required: true, message: 'Please enter card Number' }],
                                initialValue:salesMan.cardNum
                            })(
                                <Input placeholder={allStrings.cardnumber} />
                            )}
                            </Form.Item>
                            <Form.Item>
                            {getFieldDecorator('job', {
                                rules: [{ required: true, message: 'Please enter job' }],
                                initialValue:salesMan.job
                            })(
                                <Input placeholder={allStrings.job} />
                            )}
                            </Form.Item>
                            <Form.Item>
                            {getFieldDecorator('jobLocation', {
                                rules: [{ required: true, message: 'Please enter job location' }],
                                initialValue:salesMan.jobLocation
                            })(
                                <Input placeholder={allStrings.jobLocation} />
                            )}
                            </Form.Item>
                            <Form.Item>
                            {getFieldDecorator('address', {
                                rules: [{ required: false, message: 'Please enter address' }],
                                initialValue:salesMan.address
                            })(
                                <Input placeholder={allStrings.address} />
                            )}
                            </Form.Item>
                            <Form.Item>
                            {getFieldDecorator('area', {
                                rules: [{ required: false, message: 'Please enter area' }],
                                initialValue:salesMan.area
                            })(
                                <Input placeholder={allStrings.area}/>
                            )}
                            </Form.Item>
                            </Form>
                        </Modal>
                        
                    </div>
            </div>
        </div>
        </div>
        <Footer></Footer>
    </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,

  })
  
  const mapDispatchToProps = {
  }

export default connect(mapToStateProps,mapDispatchToProps) (SalesManInfo = Form.create({ name: 'normal_login' })(SalesManInfo));
