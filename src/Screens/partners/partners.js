
import React from 'react';
import Menu from '../../components/menu/menu';
import Nav from '../../components/navbar/navbar';
import Tables from '../../components/table/table';
import Footer from '../../components/footer/footer';
import './partners.css';
//import {Icon,Button,Modal,Input} from 'react-materialize';
import {Skeleton,message,Modal, Form, Icon, Input, Button,Select,Popconfirm} from 'antd';
import "antd/dist/antd.css";
import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'


class Partners extends React.Component {
  pagentationPage=0;
    counter=0;
    state = {
        modal1Visible: false,
        confirmDelete: false,
        selectedPartners:null,
        partners:[],
        loading:true

        }
        constructor(props){
          super(props)
          if(this.props.isRTL){
            allStrings.setLanguage('ar')
          }else{
            allStrings.setLanguage('en')
          }
        }
       //submit form
       flag = -1;
       getPartners = (page,deleteRow) => {
         axios.get(`${BASE_END_POINT}partners?page=${page}&limit={20}`)
         .then(response=>{
           //console.log("ALL partners")
           //console.log(response.data)
           this.setState({partners:deleteRow?response.data.data:[...this.state.partners,...response.data.data],loading:false})
         })
         .catch(error=>{
          // console.log("ALL partners ERROR")
           console.log(error.response)
         })
       }
       componentDidMount(){
        // console.log(this.props.currentUser)
         this.getPartners(1)
       }
       OKBUTTON = (e) => {
        this.deletePartners()
       }
 
       deletePartners = () => {
         let l = message.loading('Wait..', 2.5)
         axios.delete(`${BASE_END_POINT}partners/${this.state.selectedPartners}`,{
           headers: {
             'Content-Type': 'application/x-www-form-urlencoded',
             'Authorization': `Bearer ${this.props.currentUser.token}`
           },
         })
         .then(response=>{
             l.then(() => message.success('Partner Deleted', 2.5))
             this.getPartners(1,true)
             this.flag = -1
         })
         .catch(error=>{
           //  console.log(error.response)
             l.then(() => message.error('Error', 2.5))
         })
      }

    handleSubmit = (e) => {
      //
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
          if (!err) {
           // console.log('Received values of form: ', values);
            let l = message.loading('Wait..', 2.5)
        axios.post(`${BASE_END_POINT}partners`,JSON.stringify(values),{
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        })
        .then(response=>{
            l.then(() => message.success('Add Partner', 2.5));
            this.setState({ modal1Visible:false });
            this.getPartners(1,true)
            this.flag = -1
        })
        .catch(error=>{
           // console.log(error.response)
            l.then(() => message.error('Error', 2.5))
        })
          }
        });
        
      }
    //end submit form

    setModal1Visible(modal1Visible) {
      this.setState({ modal1Visible });
    }

   
//end modal
    render() {
        //form
         const { getFieldDecorator } = this.props.form;
         //select
         const Option = Select.Option;

        
         //end select

         //upload props
         const props = {
            name: 'file',
            action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
            headers: {
              authorization: 'authorization-text',
            },
            onChange(info) {
              if (info.file.status !== 'uploading') {
                //console.log(info.file, info.fileList);
              }
              if (info.file.status === 'done') {
                message.success(`${info.file.name} file uploaded successfully`);
              } else if (info.file.status === 'error') {
                message.error(`${info.file.name} file upload failed.`);
              }
            },
          };
          //end upload props
         
         let controls = (
          <Popconfirm
          title={allStrings.areusure}
          onConfirm={this.OKBUTTON}
          onCancel={this.fCANCELBUTTON}
          okText={allStrings.ok}
          cancelText={allStrings.cancel}
        >
           <Icon className='controller-icon' type="delete" />
        </Popconfirm>
       )
            let list = this.state.partners.map(val=>[val.id,""+val.name,""+val.cost,val.percentage+"%",""+val.profit,""+val.status])

          list.forEach(function(row) {
            row.push(controls)
          });
          const loadingView = [
            [<Skeleton  active/> ],
            [<Skeleton active/> ],
            [<Skeleton  active/> ],
            [<Skeleton active/> ],
            
           ]
      return (
          <div>
              <Menu></Menu>
              <Nav></Nav>
              <Tables columns={this.state.loading?['Loading...']:[allStrings.id,allStrings.name, allStrings.cost,allStrings.percentage,allStrings.profit,allStrings.status,allStrings.remove]} 
              title={allStrings.partnersTable}
              onCellClick={(colData,cellMeta,)=>{
               // console.log('col index  '+cellMeta.colIndex)
                //console.log('row index   '+colData)
                if(cellMeta.colIndex==0){
                  
                 // console.log(this.state.partners[cellMeta.rowIndex])
                  this.props.history.push('/PartnersInfo',{data:this.state.partners[this.pagentationPage+cellMeta.rowIndex]})
                }else if(cellMeta.colIndex==6){
                  const id = list[this.pagentationPage +cellMeta.rowIndex][0];
                  this.setState({selectedPartners:id})
                   // console.log(id)
                  }
              }}
               onChangePage={(currentPage)=>{
                if(currentPage>this.counter){
                  this.counter=currentPage;
                  this.pagentationPage=this.pagentationPage+10
                }else{
                 this.counter=currentPage;
                 this.pagentationPage=this.pagentationPage-10
                }
                //console.log(currentPage)
                if(currentPage%2!=0  && currentPage > this.flag){
                  this.getPartners(currentPage+1)
                  this.flag  = currentPage;
                 
                }
                  
              }}
              arr={this.state.loading?loadingView:list}></Tables>
              <div>
              <Button style={{color: 'white', backgroundColor:'#25272e', marginLeft:60}} onClick={() => this.setModal1Visible(true)}>{allStrings.addpartner}</Button>
              <Modal
                    title={allStrings.add}
                    visible={this.state.modal1Visible}
                    onOk={this.handleSubmit}
                    okText={allStrings.ok}
                    cancelText={allStrings.cancel}
                    onCancel={() => this.setModal1Visible(false)}
                  >
                    <Form onSubmit={this.handleSubmit} className="login-form">
                        <Form.Item>
                        {getFieldDecorator('name', {
                            rules: [{ required: true, message: 'Please enter name' }],
                        })(
                            <Input placeholder={allStrings.name} />
                        )}
                        </Form.Item>
                        <Form.Item>
                        {getFieldDecorator('cost', {
                            rules: [{ required: true, message: 'Please enter name' }],
                        })(
                            <Input placeholder={allStrings.cost} />
                        )}
                        </Form.Item>
                        <Form.Item>
                        {getFieldDecorator('percentage', {
                            rules: [{ required: true, message: 'Please enter percentage' }],
                        })(
                            <Input placeholder={allStrings.percentage} />
                        )}
                        </Form.Item>
                        <Form.Item>
                        {getFieldDecorator('profit', {
                            rules: [{ required: false, message: 'Please enter profit' }],
                        })(
                            <Input placeholder={allStrings.profit} />
                        )}
                        </Form.Item>
                       
                        
                    </Form>
                </Modal>
            </div>
             <Footer></Footer>
          </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,

  })
  
  const mapDispatchToProps = {
  }

  export default connect(mapToStateProps,mapDispatchToProps)  (Partners = Form.create({ name: 'normal_login' })(Partners));
  
