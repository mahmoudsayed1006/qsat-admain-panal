import React from 'react';
import Menu from '../../components/menu/menu';
import Nav from '../../components/navbar/navbar';
import Footer from '../../components/footer/footer';

import Tables from '../../components/table/table';
import './ads.css';
import { Skeleton,message,Modal, Form, Icon, Input, Button,Popconfirm} from 'antd';
import "antd/dist/antd.css";
import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux';
import {withRouter} from 'react-router-dom'
import  {allStrings} from '../../assets/strings'

class Ads extends React.Component {
    //submit form
    pagentationPage=0;
    counter=0;
    state = {
      modal1Visible: false,
      confirmDelete: false,
        selectedAds:null,
      ads:[],
      loading:true,
      file: null,
      deletedRow:null,
      }
//

      constructor(props){
        super(props)
        if(this.props.isRTL){
          allStrings.setLanguage('ar')
        }else{
          allStrings.setLanguage('en')
        }
      }
      onChange = (e) => {
        //console.log(Array.from(e.target.files))
        this.setState({file:e.target.files[0]});
       // console.log(e.target.files[0])
    }
     //submit form
     flag = -1;

     getAds = (page,deleteRow) => {
      
       axios.get(`${BASE_END_POINT}ads?page=${page}&limit={20}`)
       .then(response=>{
         //console.log("ALL ADS")
         //console.log(response.data)
         this.setState({ads:deleteRow?response.data.data:[...this.state.ads,...response.data.data],loading:false})
       })
       .catch(error=>{
        this.setState({loading:false})
         //console.log("ALL ADS ERROR")
         //console.log(error.response)
       })
     }

 
     componentDidMount(){
       
      //console.log('this.props.currentUser')
      // console.log(this.props.currentUser)
       this.getAds(1)
       var form = new FormData();
      form.append('my_field', 'my value');

      // console.log('data')
      // console.log(form)
     
     }
     OKBUTTON = (e) => {
      this.deleteAds()
     }

     deleteAds = () => {
       let l = message.loading('Wait..', 2.5)
       //console.log(this.state.selectedAds);
       axios.delete(`${BASE_END_POINT}ads/${this.state.selectedAds}`,{
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${this.props.currentUser.token}`
        },
       })
       .then(response=>{
           l.then(() => message.success('Ads Deleted', 2.5));
           this.getAds(1,true)
             this.flag = -1
           
       })
       .catch(error=>{
           //console.log(error.response)
           l.then(() => message.error('Error', 2.5))
       })
    }
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
          if (!err) {
           // console.log('Received values of form: ', values);
           // console.log(this.state.file);
            var form = new FormData();
            form.append('img',this.state.file);
            form.append('description', values.description);
            let l = message.loading('Wait..', 2.5)
            axios.post(`${BASE_END_POINT}ads`,form,{
              headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.props.currentUser.token}`
              },
            })
            .then(response=>{
                l.then(() => message.success('Add Adveristment', 2.5));
                this.setState({ modal1Visible:false });
                this.getAds(1,true)
                this.flag = -1
            })
            .catch(error=>{
                //console.log(error.response)
                l.then(() => message.error('Error', 2.5))
            })
  
       /* if(values.img.fileList.length>0){
          values.img.fileList.filter(val=>{
            data.append('img',{
              uri: val.response.url,
              type: 'multipart/form-data',
              name: 'productImages'
          }) 
          })
           
        }*/ 
        
          }
        });
        
      }
  
        
        //modal
       
        setModal1Visible(modal1Visible) {
          this.setState({ modal1Visible });
        }

    
    render() {
        //form
         const { getFieldDecorator } = this.props.form; 
          //end upload props
          let controls = (
            <Popconfirm
            title={allStrings.areusure}
            onConfirm={this.OKBUTTON}
            onCancel={this.fCANCELBUTTON}
            okText={allStrings.yes}
            cancelText={allStrings.cancel}
          >
             <Icon className='controller-icon' type="delete" />
          </Popconfirm>
         )

         let list =this.state.ads.map((val,index)=>[
          val.id,val.description,""+val.top,
          <div>

            <img src={val.img} width="50px" height="50px"></img>
          </div>,
          ""+val.visible,
          controls
        ])

        const loadingView = [
          [<Skeleton  active/> ],
          [<Skeleton active/> ],
          [<Skeleton  active/> ],
          [<Skeleton active/> ],
          
         ]

//>>>>>>> 2a9b4c6f4f3093cbfd34497e1608eb38c5ede6bf
      return (
          <div>
              <Menu></Menu>
              <Nav></Nav>
              <Tables
               columns={this.state.loading?['loading...'] :[allStrings.id,allStrings.description,allStrings.top,allStrings.image,allStrings.visible, allStrings.remove]} 
              title={allStrings.addstable}
              onCellClick={(colData,cellMeta,)=>{
                //console.log('col index  '+cellMeta.colIndex)
                //console.log('row index   '+colData)
                if(cellMeta.colIndex==0){
                  
                  //console.log(this.state.ads[cellMeta.rowIndex])
                  this.props.history.push('/AdsInfo',{data:this.state.ads[this.pagentationPage+cellMeta.rowIndex]})
                }else if(cellMeta.colIndex==5){
                  const id = list[this.pagentationPage +cellMeta.rowIndex][0];
                  this.setState({selectedAds:id,deletedRow:cellMeta.rowIndex})
                    //console.log(id)
                  }
              }}
               onChangePage={(currentPage)=>{
                if(currentPage>this.counter){
                  this.counter=currentPage;
                  this.pagentationPage=this.pagentationPage+10
                }else{
                 this.counter=currentPage;
                 this.pagentationPage=this.pagentationPage-10
                }
                //console.log(currentPage)
                if(currentPage%2!=0  && currentPage > this.flag){
                  this.getAds(currentPage+1)
                  this.flag  = currentPage;
                 
                }
                  
              }}
              arr={this.state.loading? loadingView:list}></Tables>
              <div>
              <Button style={{color: 'white', backgroundColor:'#25272e', marginLeft:60}} onClick={() => this.setModal1Visible(true)}>{allStrings.addAds}</Button>
              <Modal
                    title={allStrings.addAds}
                    visible={this.state.modal1Visible}
                    onOk={this.handleSubmit}
                    onCancel={() => this.setModal1Visible(false)}
                    okText={allStrings.ok}
                    cancelText={allStrings.cancel}
                  >
                    <Form onSubmit={this.handleSubmit} className="login-form">
                      <Form.Item>
                      {getFieldDecorator('description', {
                          rules: [{ required: true, message: 'Please enter description' }],
                      })(
                          <Input placeholder={allStrings.description}/>
                      )}
                      </Form.Item>
                      
                      <Form.Item>
                      {getFieldDecorator('img', {
                          rules: [{ required: true, message: 'Please upload img' }],
                      })(
                          <input type="file" onChange= {this.onChange} multiple></input>
                      )}
                          
                      </Form.Item>
                    </Form>
                </Modal>
                
            </div>
            <Footer></Footer>
             
          </div>
      );
    }
  }

  
  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
  })
  
  const mapDispatchToProps = {

  }

  export default  withRouter(connect(mapToStateProps,mapDispatchToProps)(Ads = Form.create({ name: 'normal_login' })(Ads)));
