import React, { Component } from 'react';
import Menu from '../../components/menu/menu';
import Nav from '../../components/navbar/navbar';
import Footer from '../../components/footer/footer';

import './user info.css';
import {Icon,Button,Card,Input,CardTitle} from 'react-materialize';
import "antd/dist/antd.css";
 import { Modal,Form ,Select ,DatePicker,message} from 'antd';
 import axios from 'axios';
 import {BASE_END_POINT} from '../../config/URL'
import {Table} from 'react-materialize'
import { connect } from 'react-redux'
import  {allStrings} from '../../assets/strings'


class UserInfo extends React.Component {
       //submit form
       state = {
        modal1Visible: false,
        user:this.props.location.state.data,
        file:null,
        //this.props.location.state.data.img[0],
        
      }

      constructor(props){
        super(props)
        if(this.props.isRTL){
          allStrings.setLanguage('ar')
        }else{
          allStrings.setLanguage('en')
        }
      }

      onChange = (e) => {
        this.setState({file:e.target.files[0]});
    }

       componentDidMount()
       {
          // console.log("ANWEr")
           console.log("USER info ",this.state.user)
          
           //this.props.location.state.data
           //console.log(this.props.location.state.data)
       }

       activeUser = (active) => {
           let uri ='';
           if(active){
            uri = `${BASE_END_POINT}${this.state.user.id}/active`
           }else{
            uri = `${BASE_END_POINT}${this.state.user.id}/disactive`
           }
          let l = message
           .loading('Wait..', 2.5)
           axios.put(uri,{},{
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${this.props.currentUser.token}`
            },
          })
           .then(response=>{
               // console.log('done')
                if(active){
                    
                    l.then(() => message.success('User Activated', 2.5))
                    
                }else{
                
                   l.then(() => message.success('User DisActivated', 2.5))
                }
                this.props.history.goBack()
           })
           .catch(error=>{
           // console.log('Error')
           // console.log(error.response)
            l.then(() => message.error('Error', 2.5))
           })
       }

     

       handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
          if (!err) {
            console.log('Received values of form: ', values);
            var form = new FormData();
            if(this.state.file){
                form.append('img',this.state.file);
            }
            form.append('firstname', values.firstname);
            form.append('lastname', values.lastname);
            form.append('email', values.email);
            form.append('phone', values.phone);
            form.append('address', values.address);
            form.append('cardNum', values.cardNum);
            form.append('area', values.area);
            form.append('type', 'CLIENT');
            let l = message.loading('Wait..', 2.5)
            axios.put(`${BASE_END_POINT}user/${this.state.user.id}/updateInfo`,form,{
              headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.props.currentUser.token}`
              },
            })
            .then(response=>{
                l.then(() => message.success('user Updated', 2.5));
                this.setState({ modal1Visible:false });
                this.props.history.goBack()
            })
            .catch(error=>{
                //console.log(error.response)
                l.then(() => message.error('Error', 2.5))
            })
          }
        });
       
        
      }

    //end submit form
      
      //modal
    
  
      setModal1Visible(modal1Visible) {
        this.setState({ modal1Visible });
      }
  

  //end modal
  
    render() {
        const { getFieldDecorator } = this.props.form;
        const {user} = this.state;
         //select
         const Option = Select.Option;

         function handleChange(value) {
            //console.log(value); 
         }
         //end select
       
      return (
          
        <div>
         <Menu></Menu>
              <Nav></Nav>
        <div className='login'>
        <div class="row">
            <div class="col m2">
                <div class='title' style={{backgroundColor:'#25272e'}}>
                    <h2 class="center-align" style={{color:'#fff'}}>{allStrings.userinfo}</h2>
                </div>
                <div class="row">
                
                    <form class="col s12">
                    <img style={{borderColor:'#25272e'}} src={user.img?user.img:"https://theimag.org/wp-content/uploads/2015/01/user-icon-png-person-user-profile-icon-20.png"}></img>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="firstname" type="text" class="validate" disabled value={user.firstname}>
                            </input>
                            <label for="firstname" class="active">{allStrings.firstname}</label>
                            </div>
                            <div class="input-field col s6">
                            <input id="lastname" type="text" class="validate" disabled value={user.lastname}></input>
                            <label for="lastname" class="active">{allStrings.lastname}</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="email" type="text" class="validate" disabled value={user.email}>
                            </input>
                            <label for="email" class="active">{allStrings.email}</label>
                            </div>
                            <div class="input-field col s6">
                            <input id="phone" type="text" class="validate" disabled value={user.phone[0]}></input>
                            <label for="phone" class="active">{allStrings.phone}</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="type" type="text" class="validate" disabled value={user.type}>
                            </input>
                            <label for="type" class="active">{allStrings.type}</label>
                            </div>
                            <div class="input-field col s6">
                            <input id="are" type="text" class="validate" disabled value={user.area}></input>
                            <label for="area" class="active">{allStrings.area}</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="cardNum" type="text" class="validate" disabled value={user.cardNum}>
                            </input>
                            <label for="cardNum" class="active">{allStrings.cardnumber}</label>
                            </div>
                            <div class="input-field col s6">
                            <input id="Address" type="text" class="validate" disabled value={user.address}></input>
                            <label for="Address" class="active">{allStrings.address}</label>
                            </div>
                        </div>
                        

                            {
                            this.state.user.type!= 'ADMIN'&&
                            <div>
                            <a class="waves-effect waves-light btn btn-large delete"  onClick={()=>this.activeUser(false)} ><i class="spcial material-icons left">close</i>{allStrings.disactive}</a>
                            <a class="waves-effect waves-light btn btn-large"  onClick={()=>this.activeUser(true)}><i class=" spcial material-icons left">done</i>{allStrings.active}</a>
                            <a class="waves-effect waves-light btn btn-large edit" onClick={() => this.setModal1Visible(true)}><i class=" spcial material-icons left">edit</i>{allStrings.edit}</a>
                            </div>
                            }                        
                            
                        </form>
                    <Modal
                            title="Edit"
                            visible={this.state.modal1Visible}
                            onOk={this.handleSubmit}
                            okText={allStrings.ok}
                            cancelText={allStrings.cancel}
                            onCancel={() => this.setModal1Visible(false)}
                        >

                            <input type="file" onChange= {this.onChange}></input>
                            <Form onSubmit={this.handleSubmit} className="login-form">
                                              
                            <Form.Item>
                            {getFieldDecorator('firstname', {
                                rules: [{ required: true, message: 'Please enter First Name' }],
                                initialValue: user.firstname,
                            })(
                                <Input placeholder={allStrings.firstname}  />
                            )}
                            </Form.Item>
                            <Form.Item>
                            {getFieldDecorator('lastname', {
                                rules: [{ required: true, message: 'Please enter last name' }],
                                initialValue:user.lastname
                            })(
                                <Input placeholder={allStrings.lastname}  />
                            )}
                            </Form.Item>
                        
                            <Form.Item>
                            {getFieldDecorator('email', {
                                rules: [{ required: true, message: 'Please enter email' }],
                                initialValue:user.email
                            })(
                                <Input placeholder={allStrings.email}  />
                            )}
                            </Form.Item>


                            <Form.Item>
                            {getFieldDecorator('phone', {
                                rules: [{ required: true, message: 'Please enter phone' }],
                                initialValue:user.phone[0]
                            })(
                                <Input placeholder={allStrings.phone}  />
                            )}
                            </Form.Item>
                            
                            <Form.Item>
                            {getFieldDecorator('cardNum', {
                                rules: [{ required: true, message: 'Please enter card Number' }],
                                initialValue:user.cardNum
                            })(
                                <Input placeholder={allStrings.cardnumber}  />
                            )}
                            </Form.Item>
                            
                            <Form.Item>
                            {getFieldDecorator('address', {
                                rules: [{ required: false, message: 'Please enter address' }],
                                initialValue:user.address
                            })(
                                <Input placeholder={allStrings.address} />
                            )}
                            </Form.Item>
                            <Form.Item>
                            {getFieldDecorator('area', {
                                rules: [{ required: false, message: 'Please enter area' }],
                                initialValue:user.area
                            })(
                                <Input placeholder={allStrings.area}/>
                            )}
                            </Form.Item>
                            </Form>
                        </Modal>
                        
                    </div>
            </div>
        </div>
        </div>
        <Footer></Footer>
    </div>
      );
    }
  }

  
  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,

  })
  
  const mapDispatchToProps = {
  }


export default connect(mapToStateProps,mapDispatchToProps) ( UserInfo = Form.create({ name: 'normal_login' })(UserInfo)) ;
