import React from 'react';
import './login.css';
import {Icon,Button,Card,Input,CardTitle} from 'react-materialize';
import {Form,Checkbox,message} from 'antd'
import "antd/dist/antd.css";
import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import {withRouter} from 'react-router-dom'
import { connect } from 'react-redux';
import {login} from '../../actions/AuthActions'

class Login extends React.Component {
    
    componentDidMount(){
       // console.log('Received values of form: ', this.props.userToken);
    }
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
          if (!err) {
            //console.log('Received values of form: ', values);
            this.props.login(values,this.props.userToken,this.props.history)
            /*let l = message.loading('Wait..', 2.5)
            axios.post(`${BASE_END_POINT}signin`,JSON.stringify(values),{
              headers: {
                'Content-Type': 'application/json',
              },
            })
            .then(response=>{
                l.then(() => message.success('done', 2.5))
            })
            .catch(error=>{
                console.log(error.response)
                l.then(() => message.error('Error', 2.5))
            })*/
          }
        });
      }
    render() {
        const { getFieldDecorator } = this.props.form;
      return (
        <div class="container">
        <div className='login'>
        <div class="row">
            <div class="col m2">
                <div class='title' style={{backgroundColor:'#25272e'}}>
                    <h2 class="center-align" style={{color:'#fff'}}>Login</h2>
                </div>
                <div class="row row-form">
                    <form class="col s12">
                    <Form onSubmit={this.handleSubmit} className="login-form">
                    <row>
                        <Form.Item style={{marginTop:'30px'}}>
                        {getFieldDecorator('email', {
                            rules: [{ required: true, message: 'Please input your email!' }],
                        })(
                            <Input placeholder="Email" type="Email" />
                        )}
                        </Form.Item>
                    
                    </row>
                    <row>
                        <Form.Item>
                        {getFieldDecorator('password', {
                            rules: [{ required: true, message: 'Please input your Password!' }],
                        })(
                            <Input  type="password" placeholder="Password" />
                        )}
                        </Form.Item>
                    </row>
                        <Form.Item>
                    <row>
                        {/*getFieldDecorator('remember', {
                            valuePropName: 'checked',
                            initialValue: true,
                        })(
                            <Checkbox style={{display:'block',float:'left',marginLeft:'10px'}}>Remember me</Checkbox>
                            
                        )*/}
                    </row>
                    <row>
                    <br></br>
                    <br></br>
                        <Button type="primary" htmlType="submit" className="login-form-button"style={{width:'100%',height:'50px',backgroundColor:'#25272e'}}>
                            Log in
                        </Button>
                    </row>
                        </Form.Item>
                    
                    </Form>
                       
                    </form>
                </div>
            </div>
        </div>
        </div>
    </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    userToken: state.auth.userToken,
  })
  
  const mapDispatchToProps = {
    login,
  }


export default  withRouter(connect(mapToStateProps,mapDispatchToProps)(Login = Form.create({ name: 'normal_login' })(Login))) ;
