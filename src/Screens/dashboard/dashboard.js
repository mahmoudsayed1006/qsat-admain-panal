import React, { Component } from 'react';
import Menu from '../../components/menu/menu';
import Nav from '../../components/navbar/navbar';
import Footer from '../../components/footer/footer';

import './dashboard.css';
import {Icon,Table} from 'react-materialize'
import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import {NavLink} from 'react-router-dom';
import { notification,Skeleton } from 'antd';
import 'antd/dist/antd.css';
import firebase from 'firebase';
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'

class Dashboard extends Component {
    componentDidMount(){
       // console.log(this.props.currentUser)
        this.getCounts()
        this.getLastAction()
            this.getLastUsers()
            this.reciveNotification()
    }
      
    reciveNotification = async () => {
        try{
          const messaging = firebase.messaging();
          await messaging.requestPermission();
           await messaging.onMessage(msg=>{
              notification.open({
                  message: msg.notification.title,
                  description: msg.notification.body,
                  icon:  <Icon>group</Icon>,
                });          
         //     console.log('user token: ', msg)
           });
          
        }catch (error) {
          //console.error(error);
        }
    }
   
        
        state = {
            users:[],
            counts:[],
            actions:[],
            loading:true,
            loading2:true
        }

        constructor(props){
            super(props)
            if(this.props.isRTL){
              allStrings.setLanguage('ar')
            }else{
              allStrings.setLanguage('en')
            }
          }
          
       //submit form
       //PENDING
       getCounts= () => {
         axios.get(`${BASE_END_POINT}admin/count`,{
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${this.props.currentUser.token}`
            },
          })
         .then(response=>{
           //console.log("ALL counts")
           //console.log(response.data)
           this.setState({counts:response.data,loading:false})
         })
         .catch(error=>{
           //console.log("ALL counts ERROR")
           //console.log(error.response)
         })
       }
       getLastUsers= () => {
        axios.get(`${BASE_END_POINT}admin/users`,{
           headers: {
             'Content-Type': 'application/json',
             'Authorization': `Bearer ${this.props.currentUser.token}`
           },
         })
        .then(response=>{
          //console.log("ALL last users")
          //console.log(response.data)
          this.setState({users:response.data,loading2:false})
        })
        .catch(error=>{
          //console.log("ALL last users ERROR")
          //console.log(error.response)
        })
      }
      getLastAction= () => {
        axios.get(`${BASE_END_POINT}admin/actions`,{
           headers: {
             'Content-Type': 'application/json',
             'Authorization': `Bearer ${this.props.currentUser.token}`
           },
         })
        .then(response=>{
          //console.log("ALL last actions")
          //console.log(response.data)
          this.setState({actions:response.data,loading2:false})
        })
        .catch(error=>{
          //console.log("ALL last orders ERROR")
         // console.log(error.response)
        })
      }
        
       
  render() {
      const {counts} = this.state;
      const {users} = this.state;
      const {actions} = this.state;
      const loadingView = 
       <Skeleton  active/> 
    return (
      <div>
        <Menu></Menu>
        <Nav></Nav>
        <div className="content">
            <div className="row">
                <div className="col s12 m6 xl4 l6 count1">
                <NavLink to='/users'>
                <div className="icons">
                    <Icon>group</Icon>
                </div>
                <div className='info'>
                    <p>{allStrings.user}</p>
                    <span>{this.state.loading?loadingView:counts.users}</span>
                </div>
                </NavLink>
                </div>
                <div className="col s12 m6 xl4 l6 count2">
                <NavLink to='/pendingOrder'>
                    <div className="icons">
                        <Icon>add_shopping_cart</Icon>
                    </div>
                    <div className='info'>
                        <p>{allStrings.order}</p>
                        <span>{this.state.loading?loadingView:counts.pending}</span>
                    </div>
                </NavLink>
                </div>
                <div className="col s12 m6 xl4 l6 count3">
                <NavLink to='/products'>
                    <div className="icons">
                        <Icon>shopping_basket</Icon>
                    </div>
                    <div className='info'>
                        <p>{allStrings.product}</p>
                        <span>{this.state.loading?loadingView:counts.products}</span>
                    </div>
                </NavLink>
                </div> 
                
            </div>
            <div className="row">
                <div className="col s12 m6 xl4 l6 count1">
                <NavLink to='/partners'>
                <div className="icons">
                    <Icon>perm_identity</Icon>
                </div>
                <div className='info'>
                    <p>{allStrings.partners}</p>
                    <span>{this.state.loading?loadingView:counts.partners}</span>
                </div>
                </NavLink>
                </div>
                <div className="col s12 m6 xl4 l6 count2">
                <NavLink to='/category'>
                    <div className="icons">
                        <Icon>turned_in_not</Icon>
                    </div>
                    <div className='info'>
                        <p>{allStrings.category}</p>
                        <span>{this.state.loading?loadingView:counts.category}</span>
                    </div>
                </NavLink>
                </div>
                <div className="col s12 m6 xl4 l6 count3">
                <NavLink to='/premiums'>
                    <div className="icons">
                        <Icon>add_to_photos</Icon>
                    </div>
                    <div className='info'>
                        <p>{allStrings.permiums}</p>
                        <span>{this.state.loading?loadingView:counts.premiums}</span>
                    </div>
                </NavLink>
                </div> 
                
            </div>
            <div className="row">
                <div className="col s12 m6 xl4 l6 count1">
                <NavLink to='/Ads'>

                <div className="icons">
                <Icon>add_to_queue</Icon>
                </div>
                <div className='info'>
                    <p>{allStrings.ads}</p>
                    <span>{this.state.loading?loadingView:counts.ads}</span>
                </div>
                </NavLink>
                </div>
                <div className="col s12 m6 xl4 l6 count2">
                    <div className="icons">
                    <Icon>attach_money</Icon>
                    </div>
                    <div className='info'>
                        <p>{allStrings.totalPermiums}</p>
                        <span>{this.state.loading?loadingView:counts.totalPremiums}</span>
                    </div>
                </div>
                <div className="col s12 m6 xl4 l6 count3">
                    <div className="icons">
                    <Icon>assessment</Icon>
                    </div>
                    <div className='info'>
                        <p>{allStrings.profit}</p>
                        <span>{this.state.loading?loadingView:counts.totalSales}</span>
                    </div>
                </div> 
                
            </div>
            
            
        </div>
        <div className='dash-table1'>
        <div className='row'>
            <div className="col s12 m6 l6 dashboard-table1">
            {this.state.loading2?loadingView:
                <Table>
                    <thead>
                        <tr>
                        <th data-field="id">{allStrings.id}</th>
                        <th data-field="name">{allStrings.name}</th>
                        <th data-field="phone">{allStrings.phone}</th>
                        <th data-field="date">{allStrings.date}</th>
                        </tr>
                    </thead>

                    <tbody>
                    {users.map(val=>(
                        <tr>
                        <td>{val.id}</td>
                        <td>{val.firstname +  val.lastname}</td>
                        <td>{val.phone[0]}</td>
                        <td>{val.createdAt.substring(0, 10)}</td>
                        </tr>
                    ))}
                    </tbody>
                </Table>
                }
            </div>
            
            <div className="col s12 m6 l6 dashboard-table1">
            {this.state.loading2?loadingView:
                <Table>
                    <thead>
                        <tr>
                        <th data-field="id">{allStrings.id}</th>
                        <th data-field="name">{allStrings.name}</th>
                        <th data-field="action">{allStrings.action}</th>
                        <th data-field="date">{allStrings.date}</th>
                        </tr>
                    </thead>

                    <tbody>
                    {actions.map(val=>(
                        <tr>
                        <td>{val.id}</td>
                        <td>{val.user.firstname +  val.user.lastname}</td>
                        <td>{val.action}</td>
                        <td>{val.createdAt.substring(0, 10)}</td>
                        </tr>
                    ))}
                      
                    </tbody>
                </Table>
            }
            </div>
            </div>
            </div>
            <Footer></Footer>
      </div>
    );
  }
}

const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,

  })
  
  const mapDispatchToProps = {
  }

export default connect(mapToStateProps,mapDispatchToProps) (Dashboard);
