import React from 'react';
import Menu from '../../components/menu/menu';
import Nav from '../../components/navbar/navbar';
import Tables from '../../components/table/table';
import Footer from '../../components/footer/footer';

import './accept order.css';
import { Icon,Popconfirm, message,Skeleton } from 'antd';
import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'


class Accept extends React.Component {
  pagentationPage=0;
    counter=0;
    state = {
        visible: false,
        confirmDelete: false,
        selectedOrder:null,
        visible: false,
        acceptedOrders:[],
        loading:true,
        }
       //submit form
       //PENDING

       constructor(props){
        super(props)
        if(this.props.isRTL){
          allStrings.setLanguage('ar')
        }else{
          allStrings.setLanguage('en')
        }
      }

       flag = -1;
       getAcceptedOrders = (page) => {
        
         axios.get(`${BASE_END_POINT}orders?status=ACCEPTED&page=${page}&limit={20}`)
         .then(response=>{
          this.setState({loading:false})
           //console.log("ALL acceptedOrders")
           //console.log(response.data.data)
           //console.log(response.data.data[0].productOrders)
           this.setState({acceptedOrders:[...this.state.acceptedOrders,...response.data.data]})
         })
         .catch(error=>{
          this.setState({loading:false})
           //console.log("ALL acceptedOrders ERROR")
           //console.log(error.response)
         })
       }
       componentDidMount(){
         this.getAcceptedOrders(1)
        // console.log(this.props.isRTL)
       }
       OKBUTTON = (e) => {
        this.deleteOrder()
       }
 
       deleteOrder = () => {
         let l = message.loading('Wait..', 2.5)
         axios.delete(`${BASE_END_POINT}orders/${this.state.selectedOrder}`,{
           headers: {
             'Content-Type': 'application/x-www-form-urlencoded',
             'Authorization': `Bearer ${this.props.currentUser.token}`
           },
         })
         .then(response=>{
             l.then(() => message.success('Order Deleted', 2.5))
         })
         .catch(error=>{
             //console.log(error.response)
             l.then(() => message.error('Error', 2.5))
         })
      }
    render() {



      let controls = (
        <Popconfirm
        title={allStrings.areusure}
        onConfirm={this.OKBUTTON}
        onCancel={this.fCANCELBUTTON}
        okText={allStrings.yes}
        cancelText={allStrings.no}
      >
         <Icon className='controller-icon' type="delete" />
      </Popconfirm>
     )
      let list = this.state.acceptedOrders.map((val,index)=>{
        
        return  [
        <p>{val.id}</p>,val.client.firstname+" "+val.client.lastname ,"mohamed",
        val.productOrders.map(v=><h6>{v.product.name}</h6>),
        val.productOrders.map(v=><h6>{v.count}</h6>),
        val.total,
        val.productOrders.map(v=><h6>{v.firstPaid}</h6>),
        val.productOrders.map(v=><h6>{v.monthCount}</h6>),
        val.productOrders.map(v=><h6>{v.costPerMonth}</h6>),
        "ismailia",
        val.createdAt.substring(0, 10)
        ]
      })        
 /* const list =[
    ["2","ahmed","mohamed",product,count,"30000","2000","5","500","ismailia","2/5/2019"],
    ["2","ahmed","mohamed",product,count,"30000","2000","5","500","ismailia","2/5/2019"] 
  ];*/
  list.forEach(function(row) {
    row.push(controls)
   });

   const loadingView = [
    [<Skeleton  active/> ],
    [<Skeleton active/> ],
    [<Skeleton  active/> ],
    [<Skeleton active/> ],
    
   ]
      return (
          <div>
              <Menu></Menu>
              <Nav></Nav>
              
              <Tables 
              columns={this.state.loading?['Loading...']:[allStrings.id,allStrings.client,allStrings.salesman,allStrings.product,allStrings.count,allStrings.total,allStrings.firstpaid,allStrings.monthcount,allStrings.costPerMonth,allStrings.Destination,allStrings.date,allStrings.remove]} 
              title={allStrings.acceptOrders}
              onCellClick={(colData,cellMeta,)=>{
                //console.log('col index  '+cellMeta.colIndex)
                //console.log('row index   '+colData)
                if(cellMeta.colIndex==0){
                  
                  //console.log(this.state.acceptedOrders[cellMeta.rowIndex])
                  this.props.history.push('/OrderInfo',{data:this.state.acceptedOrders[this.pagentationPage+cellMeta.rowIndex]})
                }else if(cellMeta.colIndex==11){
                    const id = list[this.pagentationPage +cellMeta.rowIndex][0];
                    this.setState({selectedOrder:id})
                    //console.log(id)
                  }
              }}
               onChangePage={(currentPage)=>{
                if(currentPage>this.counter){
                  this.counter=currentPage;
                  this.pagentationPage=this.pagentationPage+10
                }else{
                 this.counter=currentPage;
                 this.pagentationPage=this.pagentationPage-10
                }
                //console.log(currentPage)
                if(currentPage%2!=0  && currentPage > this.flag){
                  this.getAcceptedOrders(currentPage+1)
                  this.flag  = currentPage;
                 
                }
                  
              }}
              arr={this.state.loading?loadingView:list}
              >
             
              </Tables>
              <Footer></Footer>
          </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,

  })
  
  const mapDispatchToProps = {

  }

export default connect(mapToStateProps,mapDispatchToProps)  (Accept);
