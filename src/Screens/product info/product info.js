import React from 'react';
import Menu from '../../components/menu/menu';
import Nav from '../../components/navbar/navbar';
import Footer from '../../components/footer/footer';

import './product info.css';
import {Input,Carousel} from 'react-materialize';
import "antd/dist/antd.css";
 import { Modal,Form ,Select,message} from 'antd';
 import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux'
import  {allStrings} from '../../assets/strings'
import product from '../product/product';

class ProductInfo extends React.Component {
    state = {
         visible: false,
         editModel: false,
         product:this.props.location.state.data,
         file:null,
         once:false,
         categories:[]
         }

         constructor(props){
          super(props)
          if(this.props.isRTL){
            allStrings.setLanguage('ar')
          }else{
            allStrings.setLanguage('en')
          }
        }

         getCategories = () => {
            axios.get(`${BASE_END_POINT}categories`)
            .then(response=>{
             // console.log("ALL Categories")
              //console.log(response.data)
              this.setState({categories:response.data.data})
            })
            .catch(error=>{
              //console.log("ALL Categories ERROR")
              //console.log(error.response)
            })
          }

         onChange = (e) => {
            ///console.log(Array.from(e.target.files))
            this.setState({file:e.target.files});
           // console.log(e.target.files[0])
        }
        
         componentDidMount()
         {
             this.getCategories()
            // console.log("ANWEr")
             //console.log(this.props.currentUser)
            
             //this.props.location.state.data
             //console.log(this.props.location.state.data)
         }
    //submit form
    handleSubmit = (e) => {
        e.preventDefault();
       
        this.props.form.validateFieldsAndScroll(['offer','offerDescription'],(err, values) => {
          if (!err) {
            //console.log('Received values of form: ', values);
            this.offerProduct(values.offer,values.offerDescription)
        }
        });
      
      }

      editHandleSubmit = (e) => {
        e.preventDefault();
        const {product} = this.state
        this.props.form.validateFieldsAndScroll( ['name','cashPrice','quantity','category','description','installmentPrice','company'],(err, values) => {
          if (!err) {
            //console.log('Received values of form44: ', values);
            var form = new FormData();
           // form.append('img',this.state.file[0])
            
            if(this.state.file){
                for(var i=0 ; i<= this.state.file.length-1 ; i++)
            {
                form.append(`img`,this.state.file[i] )     
             // console.log("ooo");
            }
            }  
            
            form.append("properties",JSON.stringify(product.properties))
            //form.append('img',images);
            form.append('name', values.name);
            form.append('cashPrice', 300);
            form.append('quantity', values.quantity);
            form.append('category', values.category.key);
            form.append('description', values.description);
            form.append('installmentPrice', values.installmentPrice);
            form.append('company', values.company);
            let l = message.loading('Wait..', 2.5)
            axios.put(`${BASE_END_POINT}products/${this.state.product.id}`,form,{
              headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxIiwiaXNzIjoiYWtmYSIsImlhdCI6MTU1NjU0ODAwNTMyNCwiZXhwIjoxNTU2NTUwNTk3MzI0fQ.Fj67rxFtrO5dfJd9Jv-VNMWPs2oR9KT91kfycVgmVLI`
              },
            })
            .then(response=>{
                l.then(() => message.success('Update Product', 2.5));
                this.setState({ editModel:false });
                this.props.history.goBack()
            })
            .catch(error=>{
               // console.log(error.response)
                l.then(() => message.error('Error', 2.5))
            })
        }
        });
        
      }
    //end submit form


    showModal = () => {
      this.setState({
        visible: true,
      });
    }
  
    handleOk = (e) => {
      //console.log(e);
      this.setState({
        visible: false,
      });
    }
  
    handleCancel = (e) => {
     // console.log(e);
      this.setState({
        visible: false,
      });
    }

    deleteProduct = () => {
        let l = message.loading('Wait..', 2.5)
        axios.delete(`${BASE_END_POINT}/products/${this.state.product.id}`,{},{
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        })
        .then(response=>{
            l.then(() => message.success('Product Deleted', 2.5))
            this.props.history.goBack()

        })
        .catch(error=>{
         //   console.log(error.response)
            l.then(() => message.error('Error', 2.5))
        })
     }

    offerProduct = (offer,des) => {
      
       let l = message.loading('Wait..', 2.5)
        axios.put(`${BASE_END_POINT}products/${this.state.product.id}/offer`,JSON.stringify({
            offer:offer,
            offerDescription:des
          }),{
         headers: {
           'Content-Type': 'application/json',
           'Authorization': `Bearer ${this.props.currentUser.token}`
         },
       })
        .then(response=>{
           //  console.log('done')
             l.then(() => message.success('Product Offerd', 2.5))
             this.setState({ visible:false });
             this.props.history.goBack()
        })
        .catch(error=>{
        // console.log('Error')
        // console.log(error.response)
         l.then(() => message.error('Error', 2.5))
        })
    }

    unofferProduct = () => {
      
        let l = message.loading('Wait..', 2.5)
         axios.delete(`${BASE_END_POINT}products/${this.state.product.id}/offer`,null,{
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        })
         .then(response=>{
          //    console.log('done')
              l.then(() => message.success('Product Unofferd', 2.5))
              this.props.history.goBack()
         })
         .catch(error=>{
          //console.log('Error')
          //console.log(error.response)
          l.then(() => message.error('Error', 2.5))
         })
     }

     activeProduct = () => {
      
        let l = message.loading('Wait..', 2.5)
         axios.put(`${BASE_END_POINT}products/${this.state.product.id}/active`,{},{
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        })
         .then(response=>{
            //  console.log('done')
              
              l.then(() => message.success('Product Active', 2.5))
              this.props.history.goBack()
         })
         .catch(error=>{
          //console.log('Error')
          //console.log(error.response)
          l.then(() => message.error('Error', 2.5))
         })
     }

     disactiveProduct = () => {
      
        let l = message.loading('Wait..', 2.5)
         axios.put(`${BASE_END_POINT}products/${this.state.product.id}/dis-active`,{},{
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        })
         .then(response=>{
            //  console.log('done')
              l.then(() => message.success('Product Disactive', 2.5))
              this.props.history.goBack()
         })
         .catch(error=>{
          //console.log('Error')
          //console.log(error.response)
          l.then(() => message.error('Error', 2.5))
         })
     }

     topProduct = () => {
      
        let l = message.loading('Wait..', 2.5)
         axios.put(`${BASE_END_POINT}products/${this.state.product.id}/top`,{},{
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        })
         .then(response=>{
            //  console.log('done')
              l.then(() => message.success('Product become Top', 2.5))
              this.props.history.goBack()
         })
         .catch(error=>{
          //console.log('Error')
          //console.log(error.response)
          l.then(() => message.error('Error', 2.5))
         })
     }

     lowProduct = () => {
      
        let l = message.loading('Wait..', 2.5)
         axios.put(`${BASE_END_POINT}products/${this.state.product.id}/low`,{},{
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        })
         .then(response=>{
            //  console.log('done')
              l.then(() => message.success('Product become Low', 2.5))
              this.props.history.goBack()
         })
         .catch(error=>{
          //console.log('Error')
          //console.log(error.response)
          l.then(() => message.error('Error', 2.5))
         })
     }
  
    render() {
        const { getFieldDecorator } = this.props.form;
         //select
         const Option = Select.Option;
         const {product} = this.state

         function handleChange(value) {
            //console.log(value); 
         }
         //end select
       
      return (
          
        <div>
         <Menu></Menu>
              <Nav></Nav>
        <div className='login'>
        <div class="row">
            <div class="col m2">
                <div class='title' style={{backgroundColor:'#25272e'}}>
                    <h2 class="center-align" style={{color:'#fff'}}>{allStrings.ProductInfo}</h2>
                </div>
                <div className='row' style={{marginBottom:"0px"}}>
                <Carousel images={product.img} />
                </div>
                <div class="row" style={{marginTop:"0px"}}>
                    <form class="col s12">
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={product.name}>
                            </input>
                            <label for="name" class="active">{allStrings.name}</label>
                            </div>
                            
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="category" type="text" class="validate" disabled disabled value={product.category[0].categoryname}>
                            </input>
                            <label for="category" class="active">{allStrings.category}</label>
                            </div>
                            <div class="input-field col s6">
                            <input id="company" type="text" class="validate" disabled disabled value={product.company}></input>
                            <label for="company" class="active">{allStrings.company}</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="cashPrice" type="text" class="validate" disabled value={product.cashPrice}>
                            </input>
                            <label for="cashPrice" class="active">{allStrings.cachPrice}</label>
                            </div>
                            <div class="input-field col s6">
                            <input id="installmentPrice" type="text" class="validate" disabled value={product.installmentPrice}></input>
                            <label for="installmentPrice" class="active">{allStrings.installmentprice}</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="hasOffer" type="text" class="validate" disabled value={""+product.hasOffer}>
                            </input>
                            <label for="hasOffer" class="active">{allStrings.hasoffer}</label>
                            </div>
                            <div class="input-field col s6">
                            <input id="offer" type="text" class="validate" disabled value={product.offer}></input>
                            <label for="offer" class="active">{allStrings.offer}</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="description" type="text" class="validate" disabled disabled value={product.description}></input>
                            <label for="description" class="active">{allStrings.description}</label>
                            </div>
                            <div class="input-field col s6">
                            <input id="offerDescription" type="text" class="validate" disabled value={product.offerDescription}></input>
                            <label for="offerDescription" class="active">{allStrings.offerdescription}</label>
                            </div>
                        </div>

                        {
                          product.properties.map(prop=>(
                          prop.value&&
                          <div class="row">
                          <div class="input-field col s6">
                          <input id="description" type="text" class="validate" disabled disabled value={prop.value}></input>
                          <label for="description" class="active">{prop.attr}</label>
                          </div>
                        </div>
                        ))
                        }
                            <a class="waves-effect waves-light btn btn-large delete " style={{width: '200px'}} onClick={this.deleteProduct}><i class="material-icons left spcial">delete</i>{allStrings.remove}</a>
                            <a class="waves-effect waves-light btn btn-large delete" style={{width: '200px'}} onClick={this.unofferProduct}><i class="material-icons left spcial">clear</i>{allStrings.removeoffer}</a>
                            <a class="waves-effect waves-light btn btn-large edit" style={{width: '200px'}} onClick={this.showModal}><i class="material-icons left spcial">assistant</i>{allStrings.addoffer}</a>
                            <a class="waves-effect waves-light btn btn-large edit" style={{width: '200px'}} onClick={()=>this.setState({editModel:true})}><i class="material-icons left spcial">edit</i>{allStrings.edit}</a>
                           
                         
                            
                           <a class="waves-effect waves-light btn btn-large warning" style={{width: '200px'}} onClick={this.lowProduct}><i class="material-icons left spcial">expand_more</i>{allStrings.low}</a>
                           <a class="waves-effect waves-light btn btn-large" style={{width: '200px'}} onClick={this.topProduct}><i class="material-icons left spcial">expand_less</i>{allStrings.top}</a>
                            
                           {/*
                            <a class="waves-effect waves-light btn btn-large" style={{width: '200px'}} onClick={this.activeProduct}><i class="material-icons left spcial">done</i>{allStrings.active}</a>
                            <a class="waves-effect waves-light btn btn-large warning" style={{width: '200px'}} onClick={this.disactiveProduct}><i class="material-icons left spcial">close</i>{allStrings.disactive}</a>
                           */}

                            <div>
                                <Modal
                                title={allStrings.addoffer}
                                visible={this.state.visible}
                                onOk={this.handleSubmit}
                                onCancel={this.handleCancel}
                                okText={allStrings.ok}
                                cancelText={allStrings.cancel}
                                >
                              
                                <Form onSubmit={this.handleSubmit} className="login-form">
                                    <Form.Item>
                                    {getFieldDecorator('offer', {
                                        rules: [{ required: true, message: 'Please enter offer price' }],
                                        
                                    })(
                                        <Input placeholder={allStrings.offer}  />
                                    )}
                                    </Form.Item>

                                    <Form.Item>
                                    {getFieldDecorator('offerDescription', {
                                        rules: [{ required: true, message: 'Please enter offer description' }],
                                        
                                    })(
                                        <Input placeholder={allStrings.offerdescription}  />
                                    )}
                                    </Form.Item>

                                    

                                    </Form>
                                </Modal>

                                <Modal
                                    title={allStrings.edit}
                                    visible={this.state.editModel}
                                    onOk={this.editHandleSubmit}
                                    okText={allStrings.ok}
                                cancelText={allStrings.cancel}
                                    onCancel={() => this.setState({editModel:false})}
                                >
                                <input type="file" multiple onChange= {this.onChange}></input>
                                    <Form onSubmit={this.editHandleSubmit} className="login-form">
                                    <Form.Item>
                                    {getFieldDecorator('name', {
                                        rules: [{ required: true, message: 'Please enter productname' }],
                                        initialValue: product.name,
                                    })(
                                        <Input placeholder={allStrings.name}  />
                                    )}
                                    </Form.Item>

                                    <Form.Item>
                                    {getFieldDecorator('category', {
                                            rules: [{ required: true, message: 'Please enter category' }],
                                        })(
                                            <Select labelInValue 
                                            placeholder={allStrings.category}
                                            style={{ width: '100%'}} onChange={handleChange}>
                                            {this.state.categories.map(
                                                val=><Option value={val.id}>{val.categoryname}</Option>
                                            )}
                                            </Select>
                                        )}
                                    </Form.Item>

                                    <Form.Item>
                                    {getFieldDecorator('company', {
                                        rules: [{ required: true, message: 'Please enter company' }],
                                        initialValue:product.company
                                    })(
                                        <Input placeholder={allStrings.company}  />
                                    )}
                                    </Form.Item>
                                
                                    <Form.Item>
                                    {getFieldDecorator('installmentPrice', {
                                        rules: [{ required: true, message: 'Please enter installment price' }],
                                        initialValue:product.installmentPrice
                                    })(
                                        <Input placeholder={allStrings.installmentprice}  />
                                    )}
                                    </Form.Item>
                                    <Form.Item>
                                    {getFieldDecorator('description', {
                                        rules: [{ required: true, message: 'Please enter description' }],
                                        initialValue:product.description
                                    })(
                                        <Input placeholder={allStrings.description}  />
                                    )}
                                    </Form.Item>

                                    <Form.Item>
                                    {getFieldDecorator('quantity', {
                                        rules: [{ required: true, message: 'Please enter productname' }],
                                        initialValue: product.quantity,
                                    })(
                                        <Input placeholder={allStrings.count}  />
                                    )}
                                    </Form.Item>

                                    {/*
                                      product.properties.map((prop,i)=>(
                                        
                                        prop.value&&                       
                                        <Form.Item>
                                        {getFieldDecorator(prop.attr, {
                                        rules: [{ required: true, message: 'Please enter productname' }],
                                        initialValue: prop.value,
                                        })(
                                            <Input placeholder={prop.attr}  />
                                        )}
                                        </Form.Item>
                                        
                                      ))
                                        */}
                                    
                                    
                                   
                                    </Form>
                                </Modal>
                        
                            </div>
                        </form>
                        
                    </div>
            </div>
        </div>
        </div>
        <Footer></Footer>
    </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,

  })
  
  const mapDispatchToProps = {
  }

export default connect(mapToStateProps,mapDispatchToProps) (ProductInfo = Form.create({ name: 'normal_login', })(ProductInfo));
