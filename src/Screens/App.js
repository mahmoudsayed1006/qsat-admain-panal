import React, { Component } from 'react';
import Dashboard from './dashboard/dashboard';
import Users from './users/users';
import Partners from './partners/partners';
import Premiums from './premiums/premiums';
import Category from './category/category';
import Branch from './branch/branch';
import Ads from './Ads/ads';
import PendingOrder from './order/order';
import acceptOrder from './accept order/accept order';
import OnTheWay from './onTheWay order/onTheWay order';
import deliverdOrder from './deliverd order/deliverd order';
import Product from './product/product';
import Report from './report/report';
import Login from './login/login';
import SalesMan from './salesMan/salesMan'
import PremiumsInfo from './premiums info/premiums info'
import OrderInfo from './order info/order info'
import ProductInfo from './product info/product info'
import SalesManInfo from './salesMan info/salesMan info'
import UserInfo from './user info/user info'
import AdsInfo from './ads info/ads info'
import Splash from './splash/splash'
import CategoryInfo from './category info/category info'
import PartnersInfo from './partners info/partners info'
import OrderInfoFronNoti from './order info/OrderInfoFronNoti'
import { Route, BrowserRouter, Switch } from 'react-router-dom'
import {askForPermissioToReceiveNotifications} from '../config/push-notification';
import { Provider } from 'react-redux'
import {store,persistor } from '../store';
import { PersistGate } from 'redux-persist/integration/react'

class App extends Component {
  componentDidMount(){
   // askForPermissioToReceiveNotifications()
  }
  //
  render() {
    return (
      <Provider store={store}>
         <PersistGate loading={null} persistor={persistor}>
      <BrowserRouter>
        <div className="App">
          <Switch>
            <Route path='/Dashboard' component={Dashboard}/>
            <Route  path='/Login' component={Login}/>
            <Route exact path='/' component={Splash}/>
            <Route path='/users' component={Users} />
            <Route path='/sales-man' component={SalesMan} />
            <Route path='/premiums' component={Premiums} />
            <Route path='/Ads' component={Ads} />
            <Route path='/partners' component={Partners} />
            <Route path='/pendingOrder' component={PendingOrder} />
            <Route path='/acceptOrder' component={acceptOrder} />
            <Route path='/onTheWayOrder' component={OnTheWay} />
            <Route path='/deliverdOrder' component={deliverdOrder} />
            <Route path='/category' component={Category} />
            <Route path='/branch' component={Branch} />
            <Route path='/products' component={Product} />
            <Route path='/reports' component={Report} />
            <Route path='/PremiumsInfo' component={PremiumsInfo} />
            <Route path='/OrderInfo' component={OrderInfo} />
            <Route path='/ProductInfo' component={ProductInfo} />
            <Route path='/SalesManInfo' component={SalesManInfo} />
            <Route path='/UserInfo' component={UserInfo} />
            <Route path='/AdsInfo' component={AdsInfo} />
            <Route path='/CategoryInfo' component={CategoryInfo} />
            <Route path='/PartnersInfo' component={PartnersInfo} />
            <Route path='/OrderInfoFronNoti' component={OrderInfoFronNoti} />

          </Switch>
        </div>
      </BrowserRouter>
      </PersistGate>
      </Provider>
    );
  }
}

export default App;
