import React from 'react';
import Menu from '../../components/menu/menu';
import Nav from '../../components/navbar/navbar';
import Footer from '../../components/footer/footer';

import './ads info.css';
import {Carousel} from 'react-materialize';
import "antd/dist/antd.css";
 import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux';
import { message,Modal, Form,Input, Select} from 'antd';
import  {allStrings} from '../../assets/strings'


class AdsInfo extends React.Component {
    state = {
        modal1Visible: false,
         ads:this.props.location.state.data,
         file:null,
         }

         constructor(props){
          super(props)
          if(this.props.isRTL){
            allStrings.setLanguage('ar')
          }else{
            allStrings.setLanguage('en')
          }

          
        }

         onChange = (e) => {
          this.setState({file:e.target.files[0]});
      }
        
         componentDidMount()
         {
            // console.log("ANWEr")
            // console.log(this.props.currentUser)
            
             //this.props.location.state.data
            // console.log(this.props.location.state.data)
         }
    
    deleteAds = () => {
        let l = message.loading('Wait..', 2.5)
        axios.delete(`${BASE_END_POINT}/ads/${this.state.ads.id}`,{
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        })
        .then(response=>{
            l.then(() => message.success('ads Deleted', 2.5));
            this.props.history.goBack()
        })
        .catch(error=>{
            //console.log(error.response)
            l.then(() => message.error('Error', 2.5))
        })
     }

     activeAds = () => {
      
        let l = message.loading('Wait..', 2.5)
         axios.put(`${BASE_END_POINT}ads/${this.state.ads.id}/active`,{},{
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        })
         .then(response=>{
              //console.log('done')
              l.then(() => message.success('ads Active', 2.5))
              this.props.history.goBack()
         })
         .catch(error=>{
          //console.log('Error')
          //console.log(error.response)
          l.then(() => message.error('Error', 2.5))
         })
     }

     disactiveAds = () => {
      
        let l = message.loading('Wait..', 2.5)
         axios.put(`${BASE_END_POINT}ads/${this.state.ads.id}/dis-active`,{},{
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        })
         .then(response=>{
              //console.log('done')
              l.then(() => message.success('ads Disactive', 2.5))
              this.props.history.goBack()
         })
         .catch(error=>{
          //console.log('Error')
         // console.log(error.response)
          l.then(() => message.error('Error', 2.5))
         })
     }

     topAds = () => {
      
        let l = message.loading('Wait..', 2.5)
         axios.put(`${BASE_END_POINT}ads/${this.state.ads.id}/top`,{},{
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        })
         .then(response=>{
              //console.log('done')
              l.then(() => message.success('ads become Top', 2.5))
              this.props.history.goBack()
         })
         .catch(error=>{
          //console.log('Error')
          //console.log(error.response)
          l.then(() => message.error('Error', 2.5))
         })
     }

     lowAds = () => {
      
        let l = message.loading('Wait..', 2.5)
         axios.put(`${BASE_END_POINT}ads/${this.state.ads.id}/low`,{},{
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        })
         .then(response=>{
              //console.log('done')
              l.then(() => message.success('ads become Low', 2.5))
              this.props.history.goBack()
         })
         .catch(error=>{
          //console.log('Error')
          //console.log(error.response)
          l.then(() => message.error('Error', 2.5))
         })
     }

     handleSubmit = (e) => {
      e.preventDefault();
      this.props.form.validateFields((err, values) => {
        if (!err) {
          //console.log('Received values of form: ', values);
          var form = new FormData();
          if(this.state.file){
            form.append('img',this.state.file);
        }
        form.append('description', values.description)
        form.append('properties', values.properties.key)
        let l = message.loading('Wait..', 2.5)
        //console.log("USER info2    ",this.props.currentUser)
            axios.put(`${BASE_END_POINT}ads/${this.state.ads.id}`,form,{
              headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.props.currentUser.token}`
              },
            })
            .then(response=>{
                l.then(() => message.success('adds Updated', 2.5))
                this.props.history.goBack()
            })
            .catch(error=>{
                //console.log('edit add errror   ', error.response)
                l.then(() => message.error('Error', 2.5))
            })
        }
      });
      
    }

      
      //modal
     
      setModal1Visible(modal1Visible) {
        this.setState({ modal1Visible });
      }
    render() {
        const { getFieldDecorator } = this.props.form;
         //select
         const Option = Select.Option;
         const {ads} = this.state

         function handleChange(value) {
            //console.log(value); 
         }
         //end select
        //upload props
        const props = {
          name: 'file',
          action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
          headers: {
            authorization: 'authorization-text',
          },
          onChange(info) {
            if (info.file.status !== 'uploading') {
              //console.log(info.file, info.fileList);
            }
            if (info.file.status === 'done') {
              message.success(`${info.file.name} file uploaded successfully`);
            } else if (info.file.status === 'error') {
              message.error(`${info.file.name} file upload failed.`);
            }
          },
        };

        //end upload props
      return (
          
        <div>
         <Menu></Menu>
              <Nav></Nav>
        <div className='login'>
        <div class="row">
            <div class="col m2">
                <div class='title' style={{backgroundColor:'#25272e'}} >
                    <h2 class="center-align" style={{color:'#fff'}}>{allStrings.addinfo}</h2>
                </div>
                <div className='row' style={{marginBottom:"0px"}}>
                <Carousel images={ads.img} />
                </div>
                <div class="row" style={{marginTop:"0px"}}>
                    <form class="col s12">
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="description" type="text" class="validate" disabled value={ads.description}>
                            </input>
                            <label for="description" class="active">{allStrings.description}</label>
                            </div>
                            <div class="input-field col s6">
                            <input id="status" type="text" class="validate" disabled value={""+ads.top}></input>
                            <label for="status" class="active">{allStrings.status}</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="visible" type="text" class="validate" disabled disabled value={""+ads.visible}>
                            </input>
                            <label for="visible" class="active">{allStrings.visible}</label>
                            </div>
                            
                        </div>
                       
                            <a class="waves-effect waves-light btn btn-large delete " style={{width: '200px'}} onClick={this.deleteAds}><i class="material-icons left spcial">delete</i>{allStrings.remove}</a>
                            <a class="waves-effect waves-light btn btn-large edit" style={{width: '200px'}} onClick={() => this.setModal1Visible(true)}><i class="material-icons left spcial">edit</i>{allStrings.edit}</a>
                            <a class="waves-effect waves-light btn btn-large" style={{width: '200px'}} onClick={this.topAds}><i class="material-icons left spcial">expand_less</i>{allStrings.top}</a>
                            <a class="waves-effect waves-light btn btn-large" style={{width: '200px'}} onClick={this.activeAds}><i class="material-icons left spcial">done</i>{allStrings.active}</a>
                            <a class="waves-effect waves-light btn btn-large warning" style={{width: '200px'}} onClick={this.disactiveAds}><i class="material-icons left spcial">close</i>{allStrings.disactive}</a>
                            <a class="waves-effect waves-light btn btn-large warning" style={{width: '200px'}} onClick={this.lowAds}><i class="material-icons left spcial">expand_more</i>{allStrings.low}</a>

                            <div>
                            <Modal
                                title={allStrings.edit}
                                visible={this.state.modal1Visible}
                                onOk={this.handleSubmit}
                                onCancel={() => this.setModal1Visible(false)}
                                okText={allStrings.ok}
                                cancelText={allStrings.cancel}
                              >
                              <input type="file" onChange= {this.onChange}></input>
                                <Form onSubmit={this.handleSubmit} className="login-form">
                                  <Form.Item>
                                  {getFieldDecorator('description', {
                                      rules: [{ required: true, message: 'Please enter description' }],
                                      initialValue:ads.description
                                  })(
                                      <Input placeholder={allStrings.description} />
                                  )}
                                  </Form.Item>
                                  <Form.Item>
                                  {getFieldDecorator('properties', {
                                      rules: [{ required: true, message: 'Please enter properties' }],
                                  })(
                                      <Select labelInValue 
                                      placeholder={allStrings.type}
                                      style={{ width: '100%'}} onChange={handleChange}>
                                          <Option value="low">{allStrings.low}</Option>
                                          <Option value="top">{allStrings.top}</Option>
                                      </Select>
                                  )}
                                  </Form.Item>
                                  
                  
                                </Form>
                            </Modal>

                                
                        
                            </div>
                        </form>
                        
                    </div>
            </div>
        </div>
        </div>
        <Footer></Footer>
    </div>
      );
    }
  }

  
  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
  })
  
  const mapDispatchToProps = {

  }


export default connect(mapToStateProps,mapDispatchToProps)(AdsInfo = Form.create({ name: 'normal_login', })(AdsInfo));
