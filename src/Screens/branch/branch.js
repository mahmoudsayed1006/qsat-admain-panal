import React from 'react';
import Menu from '../../components/menu/menu';
import Nav from '../../components/navbar/navbar';
import Tables from '../../components/table/table';
import Footer from '../../components/footer/footer';

import './branch.css';
import { Skeleton,message,Modal, Form, Icon, Input, Button,Popconfirm} from 'antd';
import "antd/dist/antd.css";
import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'


class Branch extends React.Component {
  page = 0;
  pagentationPage=0;
    counter=0;
  state = {
    modal1Visible: false,
    confirmDelete: false,
    selectedBranch:null,
     branches:[],
     loading:true,
     }
    //submit form
    flag = -1;
    constructor(props){
      super(props)
      if(this.props.isRTL){
        allStrings.setLanguage('ar')
      }else{
        allStrings.setLanguage('en')
      }
    }
    getBranches = (page,deleteRow) => {
      axios.get(`${BASE_END_POINT}branch?page=${page}&limit=20`)
      .then(response=>{
       // console.log("ALL branches")
        //console.log(response.data)
        this.setState({branches:deleteRow?response.data.data:[...this.state.branches,...response.data.data],loading:false})
      })
      .catch(error=>{
       // console.log("ALL branches ERROR")
       // console.log(error.response)
        this.setState({loading:false})
      })
    }
    componentDidMount(){
      this.getBranches(1)
    }
    OKBUTTON = (e) => {
      this.deleteBranch()
     }

     deleteBranch = () => {
       let l = message.loading('Wait..', 2.5)
       axios.delete(`${BASE_END_POINT}branch/${this.state.selectedBranch}`,{
         headers: {
           'Content-Type': 'application/json',
           'Authorization': `Bearer ${this.props.currentUser.token}`
         },
       })
       .then(response=>{
           l.then(() => message.success('Branch Deleted', 2.5))
           this.getBranches(1,true)
           this.flag = -1
       })
       .catch(error=>{
          // console.log(error.response)
           l.then(() => message.error('Error', 2.5))
       })
    }
    handleSubmit = (e) => {
      //
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
          if (!err) {
           // console.log('Received values of form: ', values);
            let l = message.loading('Wait..', 2.5)
        axios.post(`${BASE_END_POINT}branch`,JSON.stringify(values),{
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        })
        .then(response=>{
            l.then(() => message.success('Add Branch', 2.5));
            this.setState({ modal1Visible:false });
            this.getBranches(1,true)
            this.flag = -1
        })
        .catch(error=>{
            //console.log(error.response)
            l.then(() => message.error('Error', 2.5))
        })
          }
        });
        
      }
     
    //end submit form

      setModal1Visible(modal1Visible) {
        this.setState({ modal1Visible });
      }

//end modal
    render() {
        //form
         const { getFieldDecorator } = this.props.form;
         let controls = (
          <Popconfirm
          title={allStrings.areusure}
          onConfirm={this.OKBUTTON}
          onCancel={this.fCANCELBUTTON}
          okText={allStrings.ok}
          cancelText={allStrings.cancel} 
        >
           <Icon className='controller-icon' type="delete" />
        </Popconfirm>
       )
            let list = this.state.branches.map(val=>[""+val.id,""+val.branchName])

          list.forEach(function(row) {
            row.push(controls)
          });
          const loadingView = [
            [<Skeleton  active/> ],
            [<Skeleton active/> ],
            [<Skeleton  active/> ],
            [<Skeleton active/> ],
            
           ]
          

      return (
          <div>
              <Menu></Menu>
              <Nav></Nav>
              <Tables columns={this.state.loading?['loading...']: [allStrings.id,allStrings.branchName,allStrings.remove]} 
              title={allStrings.branchtable}
              onCellClick={(colData,cellMeta,)=>{
                //console.log('col index  '+cellMeta.colIndex)
               // console.log('row index   '+colData)
                if(cellMeta.colIndex==0){
                  
                  //console.log(this.state.branches[cellMeta.rowIndex])
                  //this.props.history.push('/BranchInfo',{data:this.state.categories[cellMeta.rowIndex]})
                }else if(cellMeta.colIndex==2){
                  const id = list[this.pagentationPage +cellMeta.rowIndex][0];
                  this.setState({selectedBranch:id})
                    //console.log(id)
                  }
              }}
               onChangePage={(currentPage)=>{
                if(currentPage>this.counter){
                  this.counter=currentPage;
                  this.pagentationPage=this.pagentationPage+10
                }else{
                 this.counter=currentPage;
                 this.pagentationPage=this.pagentationPage-10
                }
                //console.log(currentPage)
                if(currentPage%2!=0  && currentPage > this.flag){
                  this.getBranches(currentPage+1)
                  this.flag  = currentPage;
                 
                }
                  
              }}
              arr={this.state.loading?loadingView:list}></Tables>
              <div>
              <Button style={{color: 'white', backgroundColor:'#25272e', marginLeft:60}} onClick={() => this.setModal1Visible(true)}>{allStrings.addbranch}</Button>
              <Modal
                    title={allStrings.add}
                    okText={allStrings.ok}
                    cancelText={allStrings.cancel}
                    visible={this.state.modal1Visible}
                    onOk={this.handleSubmit}
                    onCancel={() => this.setModal1Visible(false)}
                  >
                    <Form onSubmit={this.handleSubmit} className="login-form">
                        <Form.Item>
                        {getFieldDecorator('branchName', {
                            rules: [{ required: true, message: 'Please enter branch name' }],
                        })(
                            <Input placeholder={allStrings.branchName} />
                        )}
                        </Form.Item>
                        
                    </Form>
                </Modal>
               
            </div>
             <Footer></Footer>
          </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
  })
  
  const mapDispatchToProps = {
  }

  export default  connect(mapToStateProps,mapDispatchToProps)(Branch = Form.create({ name: 'normal_login' })(Branch));
